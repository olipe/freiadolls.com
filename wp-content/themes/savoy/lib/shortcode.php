<?php
function product_description_function() {
    $projects = get_field_object('blocks', 'option');
    $text = '';
    if(IsSet($projects['value'])){
        foreach($projects['value'] as $row){
            $text.=$row['elements'];
        }
    }
        return $text;
}

add_shortcode( 'product_description', 'product_description_function' );

function product_title_function() {
    global $wp_query;
    return $wp_query->queried_object->post_title;
}

add_shortcode( 'product_title', 'product_title_function' );

function product_imageID_function() {
    global $wp_query;
    return 9982;
}

add_shortcode( 'product_image', 'product_imageID_function' );