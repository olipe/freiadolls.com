<?php
	
	/* Styles
	=============================================================== */
	
	function nm_child_theme_styles() {
		 // Enqueue child theme styles
		 wp_enqueue_style( 'nm-child-theme', get_stylesheet_directory_uri() . '/style.css' );
	}
	add_action( 'wp_enqueue_scripts', 'nm_child_theme_styles', 1000 ); // Note: Use priority "1000" to include the stylesheet after the parent theme stylesheets


function nm_scripts() {
	if ( ! is_admin() ) {
		global $nm_theme_options, $nm_globals, $nm_page_includes;


		// Script path and suffix setup (debug mode loads un-minified scripts)
		if ( defined( 'NM_SCRIPT_DEBUG' ) && NM_SCRIPT_DEBUG ) {
			$script_path = NM_THEME_URI . '/js/dev/';
			$suffix = '';
		} else {
			$script_path = NM_THEME_URI . '/js/';
			$suffix = '.min';
		}


		// Enqueue scripts
		wp_enqueue_script( 'modernizr', NM_THEME_URI . '/js/plugins/modernizr.min.js', array( 'jquery' ), '2.8.3' );
		wp_enqueue_script( 'unveil', NM_THEME_URI . '/js/plugins/jquery.unveil.min.js', array( 'jquery' ), '1.0' );
		wp_enqueue_script( 'slick-slider', NM_THEME_URI . '/js/plugins/slick.min.js', array( 'jquery' ), '1.5.5' );
		wp_enqueue_script( 'magnific-popup', NM_THEME_URI . '/js/plugins/jquery.magnific-popup.min.js', array( 'jquery' ), '0.9.9' );
		wp_enqueue_script( 'nm-core', $script_path . 'nm-core' . $suffix . '.js', array( 'jquery' ), NM_THEME_VERSION );
		wp_enqueue_script( 'my-script', NM_THEME_URI . '/js/functions.js', array( 'jquery' ), NM_THEME_VERSION, true );



		// Enqueue blog-grid scripts
		if ( isset( $nm_page_includes['blog-grid'] ) )
			wp_enqueue_script( 'packery', NM_THEME_URI . '/js/plugins/packery.pkgd.min.js', array(), '2.0.0', true );


		// WP comments script
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}


		if ( nm_woocommerce_activated() ) {
			// Register shop/product scripts
			wp_register_script( 'selectod', NM_THEME_URI . '/js/plugins/selectod.custom.min.js', array( 'jquery' ), '3.8.1' );
			wp_register_script( 'nm-shop-add-to-cart', $script_path . 'nm-shop-add-to-cart' . $suffix . '.js', array( 'jquery', 'nm-shop' ), NM_THEME_VERSION );
			wp_register_script( 'nm-shop', $script_path . 'nm-shop' . $suffix . '.js', array( 'jquery', 'nm-core', 'selectod' ), NM_THEME_VERSION );
			wp_register_script( 'nm-shop-quickview', $script_path . 'nm-shop-quickview' . $suffix . '.js', array( 'jquery', 'nm-shop', 'wc-add-to-cart-variation' ), NM_THEME_VERSION );
			wp_register_script( 'nm-shop-login', $script_path . 'nm-shop-login' . $suffix . '.js', array( 'jquery' ), NM_THEME_VERSION );


			// Enqueue login script
			if ( $nm_globals['login_popup'] ) {
				wp_enqueue_script( 'nm-shop-login' );
			}


			// Enqueue shop/product scripts
			if ( isset( $nm_page_includes['products'] ) ) {
				wp_enqueue_script( 'selectod' );
				wp_enqueue_script( 'nm-shop-add-to-cart' );
				if ( $nm_theme_options['product_quickview'] ) {
					wp_enqueue_script( 'nm-shop-quickview' );
				}
			} else if ( isset( $nm_page_includes['wishlist-home'] ) ) {
				wp_enqueue_script( 'nm-shop-add-to-cart' );
			}


			// Register shop scripts
			wp_register_script( 'nm-shop-infload', $script_path . 'nm-shop-infload' . $suffix . '.js', array( 'jquery', 'nm-shop' ), NM_THEME_VERSION );
			wp_register_script( 'nm-shop-filters', $script_path . 'nm-shop-filters' . $suffix . '.js', array( 'jquery', 'nm-shop' ), NM_THEME_VERSION );
			wp_register_script( 'nm-shop-search', $script_path . 'nm-shop-search' . $suffix . '.js', array( 'jquery', 'nm-shop' ), NM_THEME_VERSION );


		// WooCommerce page - Note: Does not include the Cart, Checkout or Account pages
			// Single product page
			if ( is_product() ) {
				// Single product page: Hover image-zoom
				wp_enqueue_script( 'easyzoom', NM_THEME_URI . '/js/plugins/easyzoom.min.js', array( 'jquery' ), '2.3.0' );
				wp_enqueue_script( 'nm-shop-add-to-cart' );
				wp_enqueue_script( 'nm-shop-single-product', $script_path . 'nm-shop-single-product' . $suffix . '.js', array( 'jquery', 'nm-shop' ), NM_THEME_VERSION );
			}
			// Shop page (except Single product, Cart and Checkout)
			wp_enqueue_script( 'smartscroll', NM_THEME_URI . '/js/plugins/jquery.smartscroll.min.js', array( 'jquery' ), '1.0' );
			wp_enqueue_script( 'nm-shop-infload' );
			wp_enqueue_script( 'nm-shop-filters' );
			/*if ( $nm_globals['shop_filters_scrollbar_custom'] ) {
				wp_enqueue_script( 'nm-shop-filters-scrollbar', $script_path . 'nm-shop-filters-scrollbar' . $suffix . '.js', array( 'jquery', 'nm-shop-filters' ), NM_THEME_VERSION );
			}*/
			wp_enqueue_script( 'nm-shop-search' );
			// Cart page
			if ( is_cart() ) {
				wp_enqueue_script( 'nm-shop-cart', $script_path . 'nm-shop-cart' . $suffix . '.js', array( 'jquery', 'nm-shop' ), NM_THEME_VERSION );
			}
			// Checkout page
			else if ( is_checkout() ) {
				wp_enqueue_script( 'nm-shop-checkout', $script_path . 'nm-shop-checkout' . $suffix . '.js', array( 'jquery', 'nm-shop' ), NM_THEME_VERSION );
			}
			// Account page
			else if ( is_account_page() ) {
				wp_enqueue_script( 'nm-shop-login' );
			}
		}


		// Add local Javascript variables
		$local_js_vars = array(
			'themeUri' 				    => NM_THEME_URI,
			'ajaxUrl' 				    => admin_url( 'admin-ajax.php', 'relative' ),
			'searchUrl'				    => home_url( '?s=' ),
			'pageLoadTransition'        => intval( $nm_theme_options['page_load_transition'] ),
			'shopFiltersAjax'		    => isset( $_GET['ajax_filters'] ) ? esc_attr( $_GET['ajax_filters'] ) : esc_attr( $nm_theme_options['shop_filters_enable_ajax'] ),
			'shopAjaxUpdateTitle'	    => intval( $nm_theme_options['shop_ajax_update_title'] ),
			//'shopFilterScrollbars'	    => ( $nm_globals['shop_filters_scrollbar_custom'] ) ? 1 : 0,
			'shopImageLazyLoad'		    => intval( $nm_theme_options['product_image_lazy_loading'] ),
			'shopScrollOffset' 		    => intval( $nm_theme_options['shop_scroll_offset'] ),
			'shopScrollOffsetTablet'    => intval( $nm_theme_options['shop_scroll_offset_tablet'] ),
			'shopScrollOffsetMobile'    => intval( $nm_theme_options['shop_scroll_offset_mobile'] ),
			'shopSearch'			    => esc_attr( $nm_globals['shop_search_layout'] ),
			'shopSearchMinChar'		    => intval( $nm_theme_options['shop_search_min_char'] ),
			'shopSearchAutoClose'       => intval( $nm_theme_options['shop_search_auto_close'] ),
			'shopAjaxAddToCart'		    => ( get_option( 'woocommerce_enable_ajax_add_to_cart' ) == 'yes' && get_option( 'woocommerce_cart_redirect_after_add' ) == 'no' ) ? 1 : 0,
			'shopRedirectScroll'        => intval( $nm_theme_options['product_redirect_scroll'] ),
			'shopCustomSelect'          => intval( $nm_theme_options['product_custom_select'] ),
			'galleryZoom'               => intval( $nm_theme_options['product_image_zoom'] ),
			'galleryThumbnailsSlider'   => intval( $nm_theme_options['product_thumbnails_slider'] ),
			'shopYouTubeRelated'        => ( ! defined( 'NM_SHOP_YOUTUBE_RELATED' ) ) ? 1 : 0,
			'checkoutTacLightbox'       => intval( $nm_theme_options['checkout_tac_lightbox'] ),
			'wpGalleryPopup'            => intval( $nm_theme_options['wp_gallery_popup'] )
		);
		wp_localize_script( 'nm-core', 'nm_wp_vars', $local_js_vars );
	}
}
add_action( 'wp_footer', 'nm_scripts' ); // Add footer scripts