<?php
/**
 * PSU Uninstall
 *
 * @author      Perfect SEO url
 * @category    Core
 * @version     2.2
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit; // Exit if accessed directly
}

global $wpdb;

$api_email = get_option( 'psu_email' );
$api_key = get_option( 'psu_key' );

// Deactivate license
if ( get_option( 'psu_activated' ) === 'Activated' && $api_key !== '' && $api_email !== '' ) {
    $args = array(
        'email' => $api_email,
        'licence_key' => $api_key,
    ); 

    require_once 'perfect-seo-url.php';
    PSU()->key()->deactivate( $args );
}

// Delete options
$wpdb->query( "DELETE FROM " . $wpdb->options . " WHERE option_name LIKE 'psu_%';" );

// Remove tables
$wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . "psu_redirects" );
$wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . "psu_terms" );

// Flush rewrite rules
flush_rewrite_rules();