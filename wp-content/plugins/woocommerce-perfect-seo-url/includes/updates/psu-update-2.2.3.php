<?php
/**
 * Update PSU to 2.2.3
 *
 * @author      PSU
 * @category    Admin
 * @version     2.2.3
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Update options
update_option( 'psu_product_multiple_urls', 'no' );