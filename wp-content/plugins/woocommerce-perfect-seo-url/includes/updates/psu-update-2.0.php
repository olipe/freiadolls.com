<?php
/**
 * Update PSU to 2.0
 *
 * @author 		PSU
 * @category 	Admin
 * @version     2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Add or update options
$psu_version = get_option( 'plugin_psu_version' );
$psu_license = get_option( 'psu' );

$options = array(
	'psu_product_hierarchical_slugs' => 'yes',
	'psu_pre_install_complete' => 'yes',
	'psu_install_complete' => 'no',
	'psu_version' => $psu_version
);

if ( $psu_license !== false ) {
	$options['psu_email'] = $psu_license['activation_email'];
	$options['psu_key'] = $psu_license['api_key'];
}

foreach ( $options as $key => $value ) {
	update_option( $key, $value );
}

// Remove options
$options = array(
	'psu_category_linked_to_product',
	'psu_category_hierarchical_order',
	'plugin_psu_version',
	'psu_deactivate_checkbox',
	'psu'
);

foreach ( $options as $key ) {
	delete_option( $key );
}