<?php
/**
 * Update PSU to 1.1.6
 *
 * @author 		PSU
 * @category 	Admin
 * @version     1.1.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_option( 'psu_category_hierarchical_order', 'yes' );