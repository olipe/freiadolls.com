<?php
/**
 * Update PSU to 2.3.2
 *
 * @author      PSU
 * @category    Admin
 * @version     2.3.2
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Update options
update_option( 'psu_breadcrumb_rewrite_enabled', 'no' );
update_option( 'psu_rewrite_yoast_canonical', 'no' );

// Flush rewrite rules
flush_rewrite_rules();