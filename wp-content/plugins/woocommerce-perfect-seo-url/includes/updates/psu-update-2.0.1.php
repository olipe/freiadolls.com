<?php
/**
 * Update PSU to 2.0.1
 *
 * @author 		PSU
 * @category 	Admin
 * @version     2.0.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Fix product permalink for version 2.0
$permalinks = get_option( 'woocommerce_permalinks' );

if ( $permalinks['product_base'] === '/product/%product_cat%' || $permalinks['product_base'] === '/product' || $permalinks['product_base'] === '/' . _x( 'product', 'slug', 'woocommerce' ) ) {
    $permalinks['product_base'] = '/' . _x( 'product', 'slug', 'woocommerce' ) . '/%product_cat%';
}

if ( $permalinks['product_base'] === '/shop/%product_cat%' || $permalinks['product_base'] === '/shop' || $permalinks['product_base'] === '/' . _x( 'shop', 'default-slug', 'woocommerce' ) ) {
    $permalinks['product_base'] = '/' . _x( 'shop', 'default-slug', 'woocommerce' ) . '/%product_cat%';
}

update_option( 'woocommerce_permalinks', $permalinks );

// Remove options
$options = array(
	'psu_redirect_tag',
	'psu_tag_support',
	'psu_category_results',
	'psu_product_id',
	'psu'
);

foreach ( $options as $key ) {
	delete_option( $key );
}