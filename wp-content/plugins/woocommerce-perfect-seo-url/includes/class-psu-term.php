<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Term' ) ) :

class PSU_Term {

    const DEFAULT_TERM_GROUP = 0;
    const DEFAULT_TERM_LEVEL = 1;

    /**
     * PSU_Term instance
     */
    protected static $_instance = null;

    /**
     * Main PSU_Term instance
     * Ensures only one instance of PSU_Term is loaded or can be loaded.
     *
     * @static
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     *
     * @since 2.2
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.2' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 2.2
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.2' );
    }

    public function __construct() {
        if ( PSU()->is_activated( true ) ) {
            add_action( 'created_product_cat', array( $this, 'created' ) );
            add_action( 'edited_product_cat', array( $this, 'edited' ) );
            add_action( 'delete_product_cat', array( $this, 'delete' ) );

            add_filter( 'get_product_cat', array( $this, 'get_term' ), 10, 2 );
            add_filter( 'get_the_terms', array( $this, 'get_the_terms' ), 10, 3 );
            add_filter( 'woocommerce_get_product_terms', array( $this, 'get_the_terms' ), 10, 3 );

            add_action( 'product_cat_add_form_fields', array( $this, 'add_category_fields' ) );
            add_action( 'product_cat_edit_form_fields', array( $this, 'add_category_fields' ) );
        }
    }

    /**
     * Add term group and level
     *
     * @param int $term_id Term ID.
     * @return void
     */
    public function created( $term_id ) {
        $term = get_term( $term_id, PSU()->woocommerce_taxonomy );

        add_term_meta( $term_id, 'psu_exclude', esc_attr( $_POST['psu_exclude'] ) );

        $term_group = $term_id;
        if ( $term->parent !== 0 ) {
            $parent_term = get_term( $term->parent, PSU()->woocommerce_taxonomy );
            $term_group = $parent_term->psu_term_group;
        }

        $this->insert( $term_id, array(
            'term_group' => $term_group,
            'term_level' => $this->get_term_level( $term_id )
        ));
    }

    /**
     * Update term group and level of edited term and children
     *
     * @param int $term_id Term ID.
     * @return void
     */
    public function edited( $term_id ) {
        $term = get_term( $term_id, PSU()->woocommerce_taxonomy );

        if ( isset( $_POST['psu_exclude'] ) && ! empty( $_POST['psu_exclude'] ) ) {
            update_term_meta( $term_id, 'psu_exclude', esc_attr( $_POST['psu_exclude'] ) );
        }

        $term_group = $term_id;
        if ( $term->parent !== 0 ) {
            $parent_term = get_term( $term->parent, PSU()->woocommerce_taxonomy );
            $term_group = $parent_term->psu_term_group;
        }

        $term_children = get_term_children( $term_id, PSU()->woocommerce_taxonomy );
        $term_children[] = $term_id;

        foreach ( $term_children as $term_id ) {
            $this->update( $term_id, array(
                'term_group' => $term_group,
                'term_level' => $this->get_term_level( $term_id )
            ));
        }
    }

    /**
     * Insert term
     *
     * @param int $term_id
     * @param array $args
     *
     * @return int|boolean
     */
    public function insert( $term_id, $args = array() ) {
        global $wpdb;

        $data = array(
            'term_id' => $term_id
        );

        if ( is_array( $args ) ) {
            $data = array_merge( $data, $args );
        }

        return $wpdb->insert( $wpdb->prefix . PSU()->table_terms, $data );
    }

    /**
     * Update term
     *
     * @param int $term_id
     * @param array $args
     *
     * @return int|boolean
     */
    public function update( $term_id, $args = array() ) {
        global $wpdb;

        $where = array(
            'term_id' => $term_id
        );

        $data = $args;

        // @TODO: Check if term group and level exists. If not, do insert instead of update

        return $wpdb->update( $wpdb->prefix . PSU()->table_terms, $data, $where );
    }

    /**
     * Delete term
     *
     * @param int $term_id
     * @return int|boolean
     */
    public function delete( $term_id ) {
        global $wpdb;

        $where = array(
            'term_id' => $term_id
        );

        return $wpdb->delete( $wpdb->prefix . PSU()->table_terms, $where );
    }

    /**
     * Add PSU term_group and term_level to term
     *
     * @param int|object $term Term object or ID.
     * @param string $taxonomy The taxonomy slug.
     *
     * @return int|object
     */
    public function get_term( $term, $taxonomy ) {
        if ( $taxonomy !== PSU()->woocommerce_taxonomy ) {
            return $term;
        }

        global $wpdb;

        $term_id = $term;
        if (is_object($term)) {
            $term_id = $term->term_id;
        }

        $cache_key = 'psu_get_term_' . $term_id;

        $psu_term = wp_cache_get( $cache_key );
        if ( $psu_term === false ) {
            $psu_term = $wpdb->get_row( $wpdb->prepare( 'SELECT term_group, term_level FROM ' . $wpdb->prefix . PSU()->table_terms . ' WHERE term_id = %s', $term_id ) );
            wp_cache_set( $cache_key, $psu_term );
        }

        if ( is_null( $psu_term ) ) {
            $term->psu_term_group = self::DEFAULT_TERM_GROUP;
            $term->psu_term_level = self::DEFAULT_TERM_LEVEL;
            return $term;
        }

        $term->psu_term_group = (int) $psu_term->term_group;
        $term->psu_term_level = (int) $psu_term->term_level;

        return $term;
    }

    /**
     * Retrieve term parents
     *
     * @param int $term_id Term ID.
     * @param int $level Optional. Current term level.
     * @param array $visited Optional. Already linked to terms to prevent duplicates.
     *
     * @return int|WP_Error A list of category parents on success, WP_Error on failure.
     */
    public function get_term_level( $term_id, $level = 1, $visited = array() ) {
        $term = get_term( $term_id, PSU()->woocommerce_taxonomy );
        if ( is_wp_error( $term ) )
            return $level;

        if ( $term->parent && ( $term->parent != $term->term_id ) && !in_array( $term->parent, $visited ) ) {
            $visited[] = $term->parent;
            $level = $level + 1;
            $level = $this->get_term_level( $term->parent, $level, $visited );
        }

        return $level;
    }

    /**
     * Add PSU term_group and term_level to terms
     *
     * @param array $terms
     * @param int $post_id Post ID.
     * @param string $taxonomy The taxonomy slug.
     *
     * @return int|object
     */
    public function get_the_terms( $terms, $post_id, $taxonomy ) {
        if ( $taxonomy !== PSU()->woocommerce_taxonomy ) {
            return $terms;
        }

        global $wpdb;

        foreach ($terms as $term) {
            $cache_key = 'psu_get_term_' . $term->term_id;

            $psu_term = wp_cache_get( $cache_key );
            if ( $psu_term === false ) {
                $psu_term = $wpdb->get_row( $wpdb->prepare( 'SELECT term_group, term_level FROM ' . $wpdb->prefix . PSU()->table_terms . ' WHERE term_id = %s', $term->term_id ) );
                wp_cache_set( $cache_key, $psu_term );
            }

            if ( is_null( $psu_term ) ) {
                $term_group = self::DEFAULT_TERM_GROUP;
                $term_level = self::DEFAULT_TERM_LEVEL;
            } else {
                $term_group = (int) $psu_term->term_group;
                $term_level = (int) $psu_term->term_level;
            }

            $term->psu_term_group = $term_group;
            $term->psu_term_level = $term_level;
        }

        return $terms;
    }

    /**
     * Retrieve category parents with separator.
     *
     * @param int $id Category ID.
     * @param string $separator Optional, default is '/'. How to separate categories.
     * @param array $visited Optional. Already linked to categories to prevent duplicates.
     *
     * @return string|WP_Error A list of category parents on success, WP_Error on failure.
     */
    public function get_term_parents( $id, $separator = '/', $visited = array() ) {
        $chain = '';
        $parent = get_term( $id, PSU()->woocommerce_taxonomy );
        if ( is_wp_error( $parent ) )
            return $parent;

        $name = apply_filters( 'psu_term_parent_name', $parent->slug, $parent );

        if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
            $visited[] = $parent->parent;
            $chain .= $this->get_term_parents( $parent->parent, $separator, $visited );
        }

        $chain .= $name . $separator;
        return $chain;
    }

    /**
     * Get terms by group id
     * UNUSED FUNCTION
     *
     * @param int $term_group
     * @param array $terms
     *
     * @return array
     */
    public function get_terms_by_group( $term_group, $terms ) {
        $post_terms = array();

        foreach ( $terms as $key => $term ) {
            if ( $term->term_group !== $term_group ) {
                unset( $terms[$key] );
                continue;
            }
            $post_terms[] = $term->term_id;

            // @TODO: FEATURE - Add option to remove ancestors, so only linked categories will be shown in a product url
            $ancestors = get_ancestors( $term->term_id, PSU()->woocommerce_taxonomy );
            $post_terms = array_merge( $post_terms, $ancestors );
        }

        $post_terms = array_unique( $post_terms );

        return $post_terms;
    }

    /**
     * Add category fields
     *
     * @param object $term Term object
     */
    public function add_category_fields( $term = false ) {
        $psu_exclude = 'no';

        if ( is_object( $term ) ) {
            $psu_exclude = get_term_meta( $term->term_id, 'psu_exclude', true );
        }
        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e( 'PSU Exclude', 'psu' ); ?></label></th>
            <td>
                <select id="psu_exclude" name="psu_exclude" class="postform">
                    <option value="no" <?php selected( 'no', $psu_exclude ); ?>><?php _e( 'No', 'woocommerce' ); ?></option>
                    <option value="yes" <?php selected( 'yes', $psu_exclude ); ?>><?php _e( 'Yes', 'woocommerce' ); ?></option>
                </select>
                <p class="description"><?php _e( 'Exclude category from rewrites. Set this option to Yes if you want a page or post with the same slug to be used instead of this category.', 'psu' ); ?></p>
            </td>
        </tr>
        <?php
    }

}

endif;