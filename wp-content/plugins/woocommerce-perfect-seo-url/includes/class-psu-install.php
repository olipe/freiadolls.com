<?php
/**
 * Installation related functions and actions.
 *
 * @author 		PSU
 * @category 	Admin
 * @version     2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Install' ) ) :

/**
 * PSU_Install Class
 */
class PSU_Install {

	/**
	 * Constructor
	 */
	public function __construct() {
		// Hooks
		register_activation_hook( PSU_PLUGIN_FILE, array( $this, 'activate' ) );
		register_deactivation_hook( PSU_PLUGIN_FILE, array( $this, 'deactivate' ) );

        add_action( 'admin_init', array( $this, 'check_version' ), 5 );
		add_action( 'in_plugin_update_message-woocommerce-perfect-seo-url/perfect-seo-url.php', array( $this, 'in_plugin_update_message' ) );
		add_filter( 'plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 2 );
        add_filter( 'plugin_action_links_' . PSU_PLUGIN_BASENAME, array( $this, 'plugin_action_links' ) );
	}

	/**
	 * Activate plugin
	 *
	 * @return void
	 */
	public function activate() {
		if ( ! current_user_can( 'activate_plugins' ) ) return;

		$this->install();
		$this->update();
	}

	/**
	 * Deactive plugin
	 *
	 * @return void
	 */
	public function deactivate() {
		if ( ! current_user_can( 'activate_plugins' ) ) return;

		flush_rewrite_rules();
	}

	/**
	 * Install plugin
	 *
	 * @return void
	 */
	public function install() {
		$installed = get_option( 'psu_pre_install_complete' );
		if ( $installed === false && PSU()->is_activated() ) {
			$installed = 'yes';
		}

		if ( $installed !== 'yes' ) {
			global $wpdb;

			$sql = "CREATE TABLE " . $wpdb->prefix . PSU()->table_redirects . " (
                redirect_id bigint(20) NOT NULL AUTO_INCREMENT,
                redirect_entity_id bigint(20) NOT NULL,
                redirect_type ENUM('post', 'term', 'tag') NOT NULL DEFAULT 'post',
                redirect_url VARCHAR(255) NOT NULL,
                redirect_status smallint(3) NOT NULL DEFAULT 301,
                PRIMARY KEY  (redirect_id),
                KEY redirect_url (redirect_url)
            );
            CREATE TABLE " . $wpdb->prefix . PSU()->table_terms . " (
                term_id bigint(20) NOT NULL,
                term_group bigint(20) NOT NULL,
                term_level smallint(3) NOT NULL,
                PRIMARY KEY  (term_id),
                KEY term_group (term_group)
            );";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

			require_once( plugin_dir_path( __FILE__ ) . '/admin/class-psu-admin-api-manager-passwords.php' );
			$psu_password_management = new PSU_Admin_Password_Management();

			$instance = $psu_password_management->generate_password( 32, false );

			// Add PSU options
			$options = array(
				'psu_pre_install_complete' => 'yes',
				'psu_install_complete' => 'no',
				'psu_product_hierarchical_slugs' => 'yes',
				'psu_redirect_product' => 'yes',
				'psu_redirect_category' => 'yes',
                'psu_product_canonical_nc' => 'no',
                'psu_product_multiple_urls' => 'no',
                'psu_breadcrumb_rewrite_enabled' => 'no',
                'psu_rewrite_yoast_canonical' => 'no',
                'psu_url_suffix_enabled' => 'no',
                'psu_url_suffix' => '.html',
                'psu_general' => '',
                'psu_license' => '',
				'psu_version' => PSU()->version,
				'psu_instance' => $instance
			);

			foreach ( $options as $key => $value ) {
				add_option( $key, $value );
			}
		}
	}

	/**
	 * Handle updates
	 *
	 * @return void
	 */
	public function update() {
		$current_db_version = get_option( 'psu_version' );

		if ( $current_db_version === false ) {
			$current_db_version = get_option( 'plugin_psu_version' );
		}

		$db_updates = array(
			'1.1.0' => 'updates/psu-update-1.1.php',
			'1.1.6' => 'updates/psu-update-1.1.6.php',
			'2.0'   => 'updates/psu-update-2.0.php',
            '2.0.1' => 'updates/psu-update-2.0.1.php',
            '2.1'   => 'updates/psu-update-2.1.php',
            '2.2'   => 'updates/psu-update-2.2.php',
            '2.2.3' => 'updates/psu-update-2.2.3.php',
            '2.3.2' => 'updates/psu-update-2.3.2.php',
            '2.4'   => 'updates/psu-update-2.4.php',
            '2.5'   => 'updates/psu-update-2.5.php'
		);

		foreach ( $db_updates as $version => $updater ) {
			if ( version_compare( $current_db_version, $version, '<' ) ) {
				include( $updater );
                delete_option( 'psu_version' );
				add_option( 'psu_version', $version );
			}
		}

        delete_option( 'psu_version' );
		add_option( 'psu_version', PSU()->version );
	}

    public function check_version() {
        // @TODO: add transient
        if ( get_option( 'psu_version' ) !== PSU()->version ) {
            $this->update();
        }
    }

	public function install_finish() {
        if ( PSU()->is_installed() ) {
            echo 'Install finished';
            exit;
        }

        // Make sure a permalink structure exists
        if ( get_option( 'permalink_structure' ) === '' ) {
            update_option( 'permalink_structure', '/%postname%/' );
        }

        // Fix permalinks for versions lower than 2.0
        $permalinks = get_option( 'woocommerce_permalinks' );

        if ( $permalinks['category_base'] === '.' ) {
            $permalinks['category_base'] = '';
        }

        if ( $permalinks['tag_base'] === '.' ) {
            $permalinks['tag_base'] = '';
        }

        $permalinks['product_base'] = '/' . PSU()->woocommerce_product_slug . '/%product_cat%';

        update_option( 'woocommerce_permalinks', $permalinks );

        // Finish installation
        update_option( 'psu_install_complete', 'yes' );

        // Flush rewrite rules
        update_option( 'psu_flush_rewrite', 'yes' );
        flush_rewrite_rules();

        echo 'Install finished';
        exit;
    }

    public function install_product_redirects() {
        global $wpdb;

        $offset = intval( $_GET['offset'] ) * 100;

        $products = new WP_Query(array(
            'post_type' => PSU()->woocommerce_post_type,
            'posts_per_page' => 100,
            'offset' => $offset
        ));

        if ( $products->have_posts() ) {
            $chuncks = array_chunk( $products->posts, 10 );
            $products = null;

            foreach ( $chuncks as $posts ) {
                $values = '';
                foreach ( $posts as $post ) {
                    $values[] = $wpdb->prepare( '(%s, %s, %s)', $post->ID, 'post', get_permalink( $post->ID ) );
                }

                $query = 'INSERT INTO ' . $wpdb->prefix . PSU()->table_redirects . ' (redirect_entity_id, redirect_type, redirect_url) VALUES ';
                $query .= implode( ",\n", $values );

                $wpdb->query( $query );

                usleep( 10 );
            }
        }
        wp_reset_postdata();
        
        echo $offset + 100;
        exit;
    }

    public function install_category_redirects() {
        global $wpdb;

        $product_categories = get_terms( PSU()->woocommerce_taxonomy, array(
            'get' => 'all'
        ) );

        foreach ( $product_categories as $category ) {
            $wpdb->insert( $wpdb->prefix . PSU()->table_redirects, array(
                'redirect_entity_id' => $category->term_id,
                'redirect_type' => 'term',
                'redirect_url' => get_term_link( $category, PSU()->woocommerce_taxonomy )
            ));
        }

        echo 'ok';
        exit;
    }

    /**
     * @since 2.2
     */
    public function install_category_groups() {
        $offset = intval( $_GET['offset'] ) * 1;

        $term = get_terms( PSU()->woocommerce_taxonomy, array(
            'parent' => 0,
            'hide_empty' => false,
            'number' => 1,
            'offset' => $offset
        ));
        $term = array_shift( $term );

        $term_children = get_term_children( $term->term_id, PSU()->woocommerce_taxonomy );
        $term_children[] = $term->term_id;

        foreach ( $term_children as $term_id ) {
            PSU()->term->insert( $term_id, array(
                'term_group' => $term->term_id,
                'term_level' => PSU()->term->get_term_level( $term_id )
            ));
        }

        echo $offset + 1;
        exit;
    }

    public function install_tag_redirects() {
        global $wpdb;

        $product_tags = get_terms( PSU()->woocommerce_tag_taxonomy, array(
            'get' => 'all'
        ) );

        foreach ( $product_tags as $tag ) {
            $wpdb->insert( $wpdb->prefix . PSU()->table_redirects, array(
                'redirect_entity_id' => $tag->term_id,
                'redirect_type' => 'tag',
                'redirect_url' => str_replace( '/./', '/', get_term_link( $tag, PSU()->woocommerce_tag_taxonomy ) )
            ));
        }

        echo 'ok';
        exit;
    }

	/**
     * Show action links on the plugin screen.
     *
     * @param   mixed $links Plugin Action links
     * @return  array
     */
    public static function plugin_action_links( $links ) {
        $action_links = array(
            'settings' => '<a href="' . admin_url( 'admin.php?page=wc-psu' ) . '" title="' . esc_attr( __( 'View Perfect SEO url Settings', 'psu' ) ) . '">' . __( 'Settings', 'woocommerce' ) . '</a>'
        );

        return array_merge( $action_links, $links );
    }

    /**
     * Show row meta on the plugin screen.
     *
     * @param   mixed $links Plugin Row Meta
     * @param   mixed $file  Plugin Base file
     * @return  array
     */
    public static function plugin_row_meta( $links, $file ) {
        // @TODO: add View details link: plugin-install.php?tab=plugin-information&plugin=woocommerce-perfect-seo-url/perfect-seo-url.php&TB_iframe=true&width=600&height=550
        if ( $file == PSU_PLUGIN_BASENAME ) {
            $row_meta = array(
                'support' => '<a href="' . esc_url( 'https://www.perfectseourl.com/support/' ) . '" title="' . esc_attr( __( 'Visit Customer Support', 'psu' ) ) . '" target="_blank">' . __( 'Support', 'woocommerce' ) . '</a>'
            );

            return array_merge( $links, $row_meta );
        }

        return (array) $links;
    }

	/**
     * Show plugin changes. Code adapted from W3 Total Cache.
     */
    public static function in_plugin_update_message( $args ) {
        $version = (isset($args['version']) ? $args['version'] : (isset($args['Version']) ? $args['Version'] : '1'));
        $transient_name = 'psu_upgrade_notice_' . $version;

        if ( false === ( $upgrade_notice = get_transient( $transient_name ) ) ) {
            $response = wp_remote_get( 'https://www.perfectseourl.com/update-notice.txt' );

            if ( ! is_wp_error( $response ) && ! empty( $response['body'] ) ) {
                $upgrade_notice = self::parse_update_notice( $response['body'] );
                set_transient( $transient_name, $upgrade_notice, 43200 );
            }
        }

        echo wp_kses_post( $upgrade_notice );
    }

    /**
     * Parse update notice from readme file
     *
     * @param  string $content
     * @return string
     */
    private static function parse_update_notice( $content ) {
        // Output Upgrade Notice
        $matches        = null;
        $regexp         = '~==\s*Upgrade Notice\s*==\s*=\s*(.*)\s*=(.*)(=\s*' . preg_quote( PSU_VERSION ) . '\s*=|$)~Uis';
        $upgrade_notice = '';

        if ( preg_match( $regexp, $content, $matches ) ) {
            $version = trim( $matches[1] );
            $notices = (array) preg_split('~[\r\n]+~', trim( $matches[2] ) );

            if ( version_compare( PSU_VERSION, $version, '<' ) ) {

                $upgrade_notice .= '<div class="wc_plugin_upgrade_notice">';

                foreach ( $notices as $index => $line ) {
                    $upgrade_notice .= wp_kses_post( preg_replace( '~\[([^\]]*)\]\(([^\)]*)\)~', '<a href="${2}">${1}</a>', $line ) );
                }

                $upgrade_notice .= '</div> ';
            }
        }

        return wp_kses_post( $upgrade_notice );
    }

}

endif;

return new PSU_Install();