<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Admin_Meta_Box_Redirect' ) ) :

class PSU_Admin_Meta_Box_Redirect {

    protected $_table;

    protected $_nonce = 'psu_nonce';

    /**
     * PSU_Admin_Meta_Box_Redirect instance
     */
    protected static $_instance = null;

    /**
     * Main PSU_Admin_Meta_Box_Redirect instance
     * Ensures only one instance of PSU_Admin_Meta_Box_Redirect is loaded or can be loaded.
     *
     * @static
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     *
     * @since 2.5.1
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.5.1' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 2.5.1
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.5.1' );
    }

    public function __construct() {
        if ( PSU()->is_activated( true ) ) {
            $this->_table = PSU()->table_redirects;

            add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
            add_action( 'wp_ajax_psu-add-redirect', array( $this, 'ajax_add_redirect' ) );
            add_action( 'wp_ajax_psu-edit-redirect', array( $this, 'ajax_edit_redirect' ) );
            add_action( 'wp_ajax_psu-delete-redirect', array( $this, 'ajax_delete_redirect' ) );
        }
    }

    /**
     * Add the meta box containers.
     *
     * @param string $post_type
     */
    public function add_meta_box( $post_type ) {
        $post_types = array( 'product' );
        if ( in_array( $post_type, $post_types ) ) {
            add_meta_box(
                'psu_redirects',
                __( 'Redirects', 'psu' ),
                array( $this, 'render_meta_box_redirect' ),
                $post_type,
                'advanced',
                'high'
            );
        }
    }

    /**
     * Return redirects by ID.
     *
     * @param string $post_type
     * @return object
     */
    public function get_redirects_by_id( $id ) {
        global $wpdb;
        return $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}{$this->_table} WHERE redirect_entity_id = %s AND redirect_type LIKE 'post' ORDER BY redirect_id DESC", $id ) );
    }

    /**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function render_meta_box_redirect( $post ) {
        ?>
        <p><a class="button" id="add-redirect" href="#"><?php _e( 'Add redirect', 'psu' ); ?></a></p>

        <div id="add-redirect-form">
            <form method="post">
                <input type="hidden" id="psu_redirect_entity_id" name="psu_redirect_entity_id" value="<?php echo get_the_ID(); ?>" />
                <input type="hidden" id="psu_redirect_nonce" name="psu_redirect_nonce" value="<?php echo wp_create_nonce( $this->_nonce ); ?>" />
                <table>
                    <tr>
                        <td><label for="psu_redirect_url"><?php _e( 'Url', 'psu' ); ?></label></td>
                        <td><input type="text" id="psu_redirect_url" name="psu_redirect_url" value="<?php echo trailingslashit( get_option( 'siteurl' ) ); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="psu_redirect_status"><?php _e( 'Status', 'psu' ); ?></label></td>
                        <td><select id="psu_redirect_status" name="psu_redirect_status">
                                <option value="301"><?php _e( '301 (permanent redirect)', 'psu' ); ?></option>
                                <option value="302"><?php _e( '302 (temporary redirect)', 'psu' ); ?></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a href="#" class="cancel button-secondary alignleft"><?php _e( 'Cancel' ); ?></a>
                            <a href="#" class="save button-primary alignright"><?php _e( 'Add redirect', 'psu' ); ?></a>
                        </td>
                    </tr>
                </table>
            </form>
        </div>

        <div id="redirects-table">
            <?php echo $this->render_meta_box_redirect_table( $post->ID ); ?>
        </div>
        <?php
    }

    /**
     * Output redirect table.
     *
     * @param int $id
     */
    public function render_meta_box_redirect_table( $id ) {
        $redirects = $this->get_redirects_by_id( $id );

        if ( empty( $redirects ) ) {
            $html = __( 'No redirects found for this product.', 'psu' );
        } else {
            $html = '<table class="redirects">
            <thead>
                <tr>
                    <th>' . __( 'Url', 'psu' ) . '</th>
                    <th>' . __( 'Status', 'psu' ) . '</th>
                    <th><span class="spinner"></span></th>
                </tr>
            </thead>
            <tbody>';

            foreach ( $redirects as $redirect ) :
                $html .= '<tr data-redirect-id="' . $redirect->redirect_id . '">
                    <td><span>' . $redirect->redirect_url . '</span><input type="text" name="url" value="' . $redirect->redirect_url . '" /></td>
                    <td>
                        <span>' . $redirect->redirect_status . '</span>
                        <select name="status">
                            <option value="301">' . __( '301 (permanent redirect)', 'psu' ) . '</option>
                            <option value="302"' . ( $redirect->redirect_status == 302 ? ' selected="selected"' : '' ) . '>' . __( '302 (temporary redirect)', 'psu' ) . '</option>
                        </select>
                    </td>
                    <td>
                        <a href="#" class="delete button button-small alignright">' . __( 'Delete' ) . '</a>
                        <a href="#" class="edit button button-small alignright">' . __( 'Edit' ) . '</a>
                        <a href="#" class="save button-primary button-small alignright">' . __( 'Save' ) . '</a>
                    </td>
                </tr>';
            endforeach;

            $html .= '</tbody></table>';
        }

        return $html;
    }

    /**
     * Add redirect.
     */
    public function ajax_add_redirect() {
        $result = array(
            'status' => 'error'
        );

        // Not all required data is available
        if ( ! isset( $_POST['id'] ) || ! isset( $_POST['url'] ) || ! isset( $_POST['status'] ) || ! isset( $_POST['nonce'] ) ) {
            $result['message'] = __( 'Required data is missing.', 'psu' );
            wp_die( json_encode( $result ) );
        }

        global $wpdb;
        $id = (int) $_POST['id'];
        $url = $_POST['url'];
        $status = (int) $_POST['status'];

        // Verify that the nonce is valid
        if ( ! check_ajax_referer( $this->_nonce, 'nonce', false ) ) {
            $result['message'] = __( 'Nonce invalid.', 'psu' );
            wp_die( json_encode( $result ) );
        }

        // Check if url exists
        $data = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}{$this->_table} WHERE redirect_url = %s", $url ) );
        if ( $data ) {
            if ( $data->redirect_type === 'post' ) {
                $enitity_type = 'product';
                $entity_name = get_the_title( $data->redirect_entity_id );
            } else {
                $term = get_term( $data->redirect_entity_id, PSU()->woocommerce_taxonomy );
                $enitity_type = 'category';
                $entity_name = $term->name;
            }

            $result['message'] = __( sprintf( 'Url already exists for %s %s.', $enitity_type, $entity_name ), 'psu' );
            wp_die( json_encode( $result ) );
        }

        // Insert new redirect
        $wpdb->insert( $wpdb->prefix . $this->_table,
            array(
                'redirect_entity_id' => $id,
                'redirect_type' => 'post',
                'redirect_url' => $url,
                'redirect_status' => $status
            ),
            array(
                '%d',
                '%s',
                '%s',
                '%d'
            )
        );
        $result['status'] = 'success';
        $result['message'] = __( 'New redirect saved.', 'psu' );
        $result['html'] = $this->render_meta_box_redirect_table( $id );
        wp_die( json_encode( $result ) );
    }

    /**
     * Edit redirect.
     */
    public function ajax_edit_redirect() {
        $result = array(
            'status' => 'error'
        );

        // Not all required data is available
        if ( ! isset( $_POST['id'] ) || ! isset( $_POST['redirect_id'] ) || ! isset( $_POST['url'] ) || ! isset( $_POST['status'] ) || ! isset( $_POST['nonce'] ) ) {
            $result['message'] = __( 'Required data is missing.', 'psu' );
            wp_die( json_encode( $result ) );
        }

        global $wpdb;
        $id = (int) $_POST['id'];
        $redirect_id = (int) $_POST['redirect_id'];
        $url = $_POST['url'];
        $status = (int) $_POST['status'];

        // Verify that the nonce is valid
        if ( ! check_ajax_referer( $this->_nonce, 'nonce', false ) ) {
            $result['message'] = __( 'Nonce invalid.', 'psu' );
            wp_die( json_encode( $result ) );
        }

        // Check if url exists
        $data = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}{$this->_table} WHERE redirect_url = %s AND redirect_entity_id != %d", $url, $id ) );
        if ( $data ) {
            if ( $data->redirect_type === 'post' ) {
                $enitity_type = 'product';
                $entity_name = get_the_title( $data->redirect_entity_id );
            } else {
                $term = get_term( $data->redirect_entity_id, PSU()->woocommerce_taxonomy );
                $enitity_type = 'category';
                $entity_name = $term->name;
            }

            $result['message'] = __( sprintf( 'Url already exists for %s %s.', $enitity_type, $entity_name ), 'psu' );
            wp_die( json_encode( $result ) );
        }

        // Edit redirect
        $wpdb->update( $wpdb->prefix . $this->_table,
            array(
                'redirect_url' => $url,
                'redirect_status' => $status
            ),
            array(
                'redirect_id' => $redirect_id
            ),
            array(
                '%s',
                '%d'
            ),
            array(
                '%d'
            )
        );
        $result['status'] = 'success';
        $result['message'] = __( 'Redirect saved.', 'psu' );
        $result['html'] = $this->render_meta_box_redirect_table( $id );
        wp_die( json_encode( $result ) );
    }

    /**
     * Delete redirect.
     */
    public function ajax_delete_redirect() {
        $result = array(
            'status' => 'error'
        );

        // Not all required data is available
        if ( ! isset( $_POST['id'] ) || ! isset( $_POST['redirect_id'] ) || ! isset( $_POST['nonce'] ) ) {
            $result['message'] = __( 'Required data is missing.', 'psu' );
            wp_die( json_encode( $result ) );
        }

        global $wpdb;
        $id = (int) $_POST['id'];
        $redirect_id = (int) $_POST['redirect_id'];

        // Verify that the nonce is valid
        if ( ! check_ajax_referer( $this->_nonce, 'nonce', false ) ) {
            $result['message'] = __( 'Nonce invalid.', 'psu' );
            wp_die( json_encode( $result ) );
        }

        // Delete redirect
        $wpdb->delete( $wpdb->prefix . $this->_table,
            array(
                'redirect_id' => $redirect_id
            ),
            array(
                '%d'
            )
        );
        $result['status'] = 'success';
        $result['message'] = __( 'Redirect removed.', 'psu' );
        $result['html'] = $this->render_meta_box_redirect_table( $id );
        wp_die( json_encode( $result ) );
    }

}

endif;