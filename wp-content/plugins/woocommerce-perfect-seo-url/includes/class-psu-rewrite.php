<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Rewrite' ) ) :

class PSU_Rewrite {

    /**
     * PSU_Rewrite instance
     */
    protected static $_instance = null;

    /**
     * Main PSU_Rewrite instance
     * Ensures only one instance of PSU_Rewrite is loaded or can be loaded.
     *
     * @static
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     *
     * @since 2.1.1
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.1.1' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 2.1.1
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.1.1' );
    }

    public function __construct() {
        if ( PSU()->is_activated( true ) ) {
            add_action( 'created_product_cat', array( $this, 'schedule_flush' ) );
            add_action( 'edited_product_cat', array( $this, 'schedule_flush' ) );
            add_action( 'delete_product_cat', array( $this, 'schedule_flush' ) );
            add_filter( 'rewrite_rules_array', array( $this, 'product_cat_rewrite_rules' ), 99 );
        }
    }

    /**
     * Save an option that triggers a flush on the next init.
     */
    public function schedule_flush() {
        update_option( 'psu_flush_rewrite', 'yes' );
    }

    /**
     * This function taken and only slightly adapted from WP No Category Base plugin by Saurabh Gupta
     *
     * @return array
     */
    public function product_cat_rewrite_rules( $rules ) {
        global $wp_rewrite;

        wp_cache_flush();

        $category_rewrites = array();
        $product_rewrites = array();
        $taxonomy = get_taxonomy( PSU()->woocommerce_taxonomy );
        $woocommerce_permalinks = get_option( 'woocommerce_permalinks' );

        /**
         * Filter: 'psu_rewrite_url_suffix' - Allow filtering of url suffix
         */
        $url_suffix = apply_filters( 'psu_rewrite_url_suffix', '/' );

        // Category prefix
        $category_base = $woocommerce_permalinks['category_base'];
        if ( $category_base !== '' ) {
            $category_base = trailingslashit( $category_base );
        }

        $category_prefix = $category_base;

        // Product prefix
        $product_base = $woocommerce_permalinks['product_base'];

        $find = array(
            '/' . _x( 'product', 'slug', 'woocommerce' ),
            '/product',
            '/%product_cat%'
        );

        $product_base = trim( str_replace( $find, '', $product_base ), '/' );

        $product_prefix = '';
        if ( $product_base !== '' ) {
            $product_prefix = trailingslashit( $product_base );
        }

        /**
         * Filter: 'psu_rewrite_get_categories' - Allow filtering of get_categories
         */
        $categories = apply_filters( 'psu_rewrite_get_categories', get_categories( array( 'taxonomy' => PSU()->woocommerce_taxonomy, 'hide_empty' => false ) ) );

        if ( is_array( $categories ) ) {
            foreach ( $categories as $category ) {
                $category_nicename = apply_filters( 'psu_rewrite_category_nicename', $category->slug, $category );

                if ( get_option( 'psu_product_hierarchical_slugs' ) === 'yes' ) {
                    if ( $category->parent == $category->term_id ) {
                        // recursive recursion
                        $category->parent = 0;
                    }
                    elseif ( $taxonomy->rewrite['hierarchical'] != 0 && $category->parent != 0 ) {
                        $parents = $this->get_category_parents( $category->parent, '/' );
                        if ( ! is_wp_error( $parents ) ) {
                            $category_nicename = $parents . $category_nicename;
                        }
                        unset( $parents );
                    }
                }

                $psu_exclude = get_term_meta( $category->term_id, 'psu_exclude', true );

                if ( $psu_exclude !== 'yes' ) {
                    $category_rewrites[$category_prefix . $category_nicename . $url_suffix . '?$'] = 'index.php?product_cat=' . $category->slug;
                    $category_rewrites[$category_prefix . $category_nicename . '/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?product_cat=' . $category->slug . '&feed=$matches[1]';
                    $category_rewrites[$category_prefix . $category_nicename . '/' . $wp_rewrite->pagination_base . '/?([0-9]{1,})' . $url_suffix . '?$'] = 'index.php?product_cat=' . $category->slug . '&paged=$matches[1]';
                }

                $category_rewrites = apply_filters( 'psu_rewrite_category_rewrites', $category_rewrites, $category, $category_prefix, $category_nicename, $url_suffix );

                $product_rewrites[$product_prefix . $category_nicename . '/([^/]+)' . $url_suffix . '?$'] = 'index.php?product=$matches[1]';
                // $product_rewrites[$product_prefix . $category_nicename . '/([^/]+)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?product=$matches[1]&feed=$matches[2]';

                $product_rewrites = apply_filters( 'psu_rewrite_product_rewrites', $product_rewrites, $category, $product_prefix, $category_nicename, $url_suffix );
            }
            unset( $categories, $category, $category_nicename );
        }

        return $category_rewrites + $product_rewrites + $rules;
    }

    /**
     * Retrieve category parents with separator.
     *
     * @param int $id Category ID.
     * @param string $separator Optional, default is '/'. How to separate categories.
     * @param array $visited Optional. Already linked to categories to prevent duplicates.
     * @return string|WP_Error A list of category parents on success, WP_Error on failure.
     */
    public function get_category_parents( $id, $separator = '/', $visited = array() ) {
        $chain = '';
        $parent = get_term( $id, PSU()->woocommerce_taxonomy );
        if ( is_wp_error( $parent ) )
            return $parent;

        $name = apply_filters( 'psu_rewrite_category_parent_name', $parent->slug, $parent );

        if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
            $visited[] = $parent->parent;
            $chain .= $this->get_category_parents( $parent->parent, $separator, $visited );
        }

        $chain .= $name . $separator;

        return $chain;
    }

}

endif;