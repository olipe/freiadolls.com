<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Compatibility_Sitepress' ) ) :

class PSU_Compatibility_Sitepress {

    public function __construct() {
        add_filter( 'psu_product_category_slug', array( $this, 'psu_product_category_slug' ) );
    }

    public function psu_product_category_slug() {
        return 'product-category';
    }

}

endif;

new PSU_Compatibility_Sitepress();