<?php
/**
 * Plugin Name: WooCommerce Perfect SEO Url
 * Plugin URI: https://www.perfectseourl.com
 * Description: Adds perfect SEO urls for products and product categories.
 * Version: 2.6.7
 * Author: Perfect SEO Url
 * Author URI: https://www.perfectseourl.com
 * Requires at least: 4.4
 * Tested up to: 4.6.1
 *
 * Text Domain: psu
 * Domain Path: /languages/
 *
 * @package PSU
 * @category Core
 * @author Perfect SEO Url
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU' ) ) :

/**
 * Main Perfect SEO Url Class
 *
 * @class PSU
 * @version 2.6.7
 */
final class PSU {

    /**
     * PSU version
     */
    public $version = '2.6.7';

    /**
     * PSU product ID
     */
    public $product_id = 'WooCommerce Perfect SEO url';

    /**
     * PSU instance
     */
    protected static $_instance = null;

    /**
     * Database table name for redirects
     */
    public $table_redirects = 'psu_redirects';

    /**
     * Database table name for terms
     */
    public $table_terms = 'psu_terms';

    /**
     * Woocommerce post type
     */
    public $woocommerce_post_type = 'product';

    /**
     * Woocommerce category taxonomy
     */
    public $woocommerce_taxonomy = 'product_cat';

    /**
     * Woocommerce tag taxonomy
     */
    public $woocommerce_tag_taxonomy = 'product_tag';

    /**
     * Woocommerce product slug
     */
    public $woocommerce_product_slug;

    /**
     * Woocommerce product category slug
     */
    public $woocommerce_product_category_slug;

    /**
     * PSU update url
     */
    public $upgrade_url = 'https://www.perfectseourl.com';

    /**
     * PSU plugin name
     */
    public $plugin_name = null;

    /**
     * Current category
     */
    protected $current_category = null;

    /**
     * Current post
     */
    protected $current_post = null;

    /**
     * Instances
     */
    public $term;
    public $rewrite;
    public $breadcrumb;
    public $url_suffix;

    /**
     * Admin Instances
     */
    public $admin;
    public $admin_meta_box_redirect;

    /**
     * Main PSU instance
     * Ensures only one instance of PSU is loaded or can be loaded.
     *
     * @static
     */
    public static function instance() {
        if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) && ! ( self::$_instance instanceof PSU ) ) {
            self::$_instance = new self();
            if ( is_admin() ) {
                if ( ! is_object( self::$_instance->admin ) ) {
                    self::$_instance->admin = PSU_Admin::instance();
                }
                if ( ! is_object( self::$_instance->admin_meta_box_redirect ) ) {
                    self::$_instance->admin_meta_box_redirect = PSU_Admin_Meta_Box_Redirect::instance();
                }
            }
            if ( ! is_object( self::$_instance->rewrite ) ) {
                self::$_instance->rewrite = PSU_Rewrite::instance();
            }
            if ( ! is_object( self::$_instance->term ) ) {
                self::$_instance->term = PSU_Term::instance();
            }
            if ( ! is_object( self::$_instance->breadcrumb ) && get_option( 'psu_breadcrumb_rewrite_enabled' ) === 'yes' ) {
                self::$_instance->breadcrumb = PSU_Breadcrumb::instance();
            }
            if ( ! is_object( self::$_instance->url_suffix ) && get_option( 'psu_url_suffix_enabled' ) === 'yes' ) {
                self::$_instance->url_suffix = PSU_Url_Suffix::instance();
            }
        }
        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     *
     * @since 1.0
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '1.0' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 1.0
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '1.0' );
    }

    /**
     * PSU Constructor
     *
     * @return PSU
     */
    public function __construct() {
        $this->plugin_name = untrailingslashit( plugin_basename( __FILE__ ) );

        $this->define_constants();
        $this->includes();

        add_action( 'init', array( $this, 'init' ), 0 );
        add_action( 'init', array( $this, 'init_default' ), 10 );
        add_action( 'init', array( $this, 'flush' ), 999 );
        add_filter( 'wp_die_ajax_handler', array( $this, 'flush' ) );
    }

    /**
     * Define PSU Constants
     */
    private function define_constants() {
        define( 'PSU_VERSION', $this->version );
        define( 'PSU_PLUGIN_FILE', __FILE__ );
        define( 'PSU_PLUGIN_BASENAME', plugin_basename( PSU_PLUGIN_FILE ) );
        define( 'PSU_PLUGIN_PATH', plugin_dir_path( PSU_PLUGIN_FILE ) );
        define( 'PSU_PLUGIN_DIR', plugin_dir_url( PSU_PLUGIN_FILE ) );
        define( 'PSU_PLUGIN_DIR_NAME', dirname( plugin_basename( PSU_PLUGIN_FILE ) ) );
    }

    /**
     * Load Localisation files.
     */
    public function load_plugin_textdomain() {
        load_plugin_textdomain( 'woocommerce', false, 'woocommerce/i18n/languages' );
    }

    /**
     * Include required core files used on the frontend
     */
    private function includes() {
        require_once PSU_PLUGIN_PATH . 'includes/class-psu-install.php';
        require_once PSU_PLUGIN_PATH . 'includes/class-psu-term.php';
        require_once PSU_PLUGIN_PATH . 'includes/class-psu-breadcrumb.php';
        require_once PSU_PLUGIN_PATH . 'includes/class-psu-url-suffix.php';
        require_once PSU_PLUGIN_PATH . 'includes/class-psu-rewrite.php';

        if ( is_admin() ) {
            $this->admin_includes();
        }

        require_once PSU_PLUGIN_PATH . 'includes/class-psu-compatibility.php';
    }

    /**
     * Include required core files used in admin
     */
    private function admin_includes() {
        require_once PSU_PLUGIN_PATH . 'includes/admin/class-psu-admin.php';
        require_once PSU_PLUGIN_PATH . 'includes/admin/class-psu-admin-meta-box-redirect.php';
        require_once PSU_PLUGIN_PATH . 'includes/admin/class-psu-admin-key-api.php';
        require_once PSU_PLUGIN_PATH . 'includes/admin/class-psu-admin-plugin-update.php';
    }

    /**
     * Init PSU when WordPress initialises
     */
    public function init() {
        // Set up localisation
        $this->load_plugin_textdomain();

        if ( $this->is_activated( true ) ) {
            remove_action( 'wp_head', 'rel_canonical' );

            add_filter( 'post_type_link', array( $this, 'post_type_link' ), 1, 4 );
            add_filter( 'term_link', array( $this, 'term_link' ), 1, 3 );
            add_filter( 'request', array( $this, 'request' ) );
            add_action( 'parse_query', array( $this, 'parse_query' ) );

            if ( get_option( 'psu_product_canonical_nc' ) === 'yes' ) {
                add_filter( 'wpseo_xml_sitemap_post_url', array( $this, 'wpseo_xml_sitemap_post_url' ), 10, 2 );
            }

            if ( get_option( 'psu_rewrite_yoast_canonical' ) === 'yes' ) {
                add_filter( 'wpseo_canonical', array( $this, 'canonical' ), 10, 1 );
            } else {
                add_action( 'wp_head', array( $this, 'canonical' ), 10 );
            }
        }
    }

    /**
     * Set some variables
     */
    public function init_default() {
        $this->woocommerce_product_slug = apply_filters( 'psu_product_slug', _x( 'product', 'slug', 'woocommerce' ) );
        $this->woocommerce_product_category_slug = apply_filters( 'psu_product_category_slug', _x( 'product-category', 'slug', 'woocommerce' ) );
    }

    /**
     * If the flush option is set, flush the rewrite rules.
     */
    public function flush( $callback ) {
        if ( get_option( 'psu_flush_rewrite' ) ) {
            add_action( 'shutdown', 'flush_rewrite_rules' );
            delete_option( 'psu_flush_rewrite' );
        }

        return $callback;
    }

    /**
     * API Key Class.
     *
     * @return PSU_Admin_Key
     */
    public function key() {
        return PSU_Admin_Key::instance();
    }

    /**
     * Filter to allow product_cat in the permalinks for products
     *
     * @param string $permalink
     * @param object $post
     * @param boolean $leavename
     * @param boolean $sample
     *
     * @return string
     */
    public function post_type_link( $permalink, $post, $leavename, $sample ) {
        if ( $post->post_type !== $this->woocommerce_post_type )
            return $permalink;

        /**
         * Filter: 'psu_post_type_link_before' - Allow filtering of permalink
         */
        $permalink = apply_filters( 'psu_post_type_link_before', $permalink, $post, $leavename, $sample );

        $product_base = trailingslashit( $this->woocommerce_product_slug );

        // Product categories
        $terms = get_the_terms( $post->ID, $this->woocommerce_taxonomy );

        $woocommerce_permalinks = get_option( 'woocommerce_permalinks' );
        if ( strpos( $woocommerce_permalinks['product_base'], '%product_cat%' ) === false ) {
            $terms = null;
        } else {
            // Fix for products that don't have any categories when %product_cat% is enabled
            if ( empty( $terms ) )
                $product_base = '/' . $product_base;
        }

        if ( empty( $terms ) ) {
            $product_cat = '';
        } else {
            if ( ! is_null( $this->current_category ) && get_option( 'psu_product_multiple_urls' ) === 'yes' ) {
                $product_cat = $this->current_category->slug;

                if ( get_option( 'psu_product_hierarchical_slugs' ) === 'yes' ) {
                    $product_cat = untrailingslashit( PSU()->term->get_term_parents( $this->current_category->term_id ) );
                }
            } else {
                usort( $terms, function( $a, $b ) {
                    return $a->psu_term_level - $b->psu_term_level;
                });

                $first_term = end( $terms );

                /**
                 * Filter: 'psu_post_type_link_replace_term' - Allow filtering of the first term
                 */
                $first_term = apply_filters( 'psu_post_type_link_replace_term', $first_term, $terms );

                $product_cat = $first_term->slug;

                if ( get_option( 'psu_product_hierarchical_slugs' ) === 'yes' ) {
                    $product_cat = untrailingslashit( PSU()->term->get_term_parents( $first_term->term_id ) );
                }
            }
        }

        // @TODO: Add this string replace to new function (strip_product_base?)
        $find = array(
            $product_base,
            '/product/',
            '%product_cat%'
        );

        $replace = array(
            '',
            '/',
            $product_cat
        );

        $permalink = str_replace( $find, $replace, $permalink );

        /**
         * Filter: 'psu_post_type_link_after' - Allow filtering of permalink
         */
        $permalink = apply_filters( 'psu_post_type_link_after', $permalink, $post, $leavename, $sample );

        return $permalink;
    }

    /**
     * Override the category link to remove the category base
     *
     * @param string $termlink
     * @param object $term
     * @param string $taxonomy
     *
     * @return string
     */
    public function term_link( $termlink, $term, $taxonomy ) {
        if ( $taxonomy !== $this->woocommerce_taxonomy )
            return $termlink;

        /**
         * Filter: 'psu_term_link_before' - Allow filtering of termlink
         */
        $termlink = apply_filters( 'psu_term_link_before', $termlink, $term, $taxonomy );

        $woocommerce_permalinks = get_option( 'woocommerce_permalinks' );

        /**
         * Filter: 'psu_term_link_category_base' - Allow filtering of term category base
         */
        $category_base = apply_filters( 'psu_term_link_category_base', $woocommerce_permalinks['category_base'] );

        if ( $category_base === '' ) {
            $category_base = $this->woocommerce_product_category_slug;
        }

        $category_base = trailingslashit( $category_base );

        // Don't remove category_base for custom values
        if ( $category_base !== trailingslashit( $this->woocommerce_product_category_slug ) ) {
            $category_base = '';
        }

        if ( get_option( 'psu_product_hierarchical_slugs' ) === 'no' ) {
            if ( $category_base === trailingslashit( $this->woocommerce_product_category_slug ) ) {
                $category_base = '';
            }
            $termlink = home_url( trailingslashit( $category_base . $term->slug ) );
        } else {
            $termlink = str_replace( $category_base, '', $termlink );
        }

        /**
         * Filter: 'psu_term_link_after' - Allow filtering of termlink
         */
        $termlink = apply_filters( 'psu_term_link_after', $termlink, $term, $taxonomy );

        return $termlink;
    }

    /**
     * Set product query vars
     * If old url is found, redirect to new url
     *
     * @param array $request
     * @return array
     */
    public function request( $request ) {
        if ( is_admin() )
            return $request;

        global $wp, $wpdb;

        $is_paged = false;
        $is_feed = false;

        /**
         * Filter: 'psu_request_filter_url_before' - Allow filtering of request URL
         */
        $url = apply_filters( 'psu_request_filter_url_before', $wp->request );

        if ( empty( $url ) )
            return $request;

        if ( isset( $_SERVER['REQUEST_SCHEME'] ) ) {
            $this->redirect( $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
        } else {
            $this->redirect( home_url( user_trailingslashit( $url ) ) );
        }

        // @TODO: Cache request for faster response

        $url = explode( '/', $url );

        if ( end( $url ) === 'feed' ) {
            $is_feed = true;

            $url = array_reverse( $url );
            $url = $url[1];
        } else {
            $url = end( $url );
        }

        /**
         * Filter: 'psu_request_filter_url_after' - Allow filtering of request URL
         */
        $url = apply_filters( 'psu_request_filter_url_after', $url, $is_paged, $is_feed );

        /**
         * Filter: 'psu_request_query_select_product' - Allow filtering of query
         */
        $query = apply_filters( 'psu_request_query_select_product', $wpdb->prepare( "SELECT ID, post_type FROM {$wpdb->posts} WHERE post_name = %s AND post_type IN (%s, %s, %s) AND post_status = %s", $url, 'post', 'page', $this->woocommerce_post_type, 'publish' ), $url );

        $post = $wpdb->get_results( $query );

        if ( count( $post ) === 1 && $post[0]->post_type === $this->woocommerce_post_type ) {
            $this->current_post = $post[0];
            $request = array(
                'post_type' => $this->woocommerce_post_type,
                'product' => $url,
                'name' => $url
            );

            if ( $is_feed ) {
                $request['feed'] = 'feed';
            }
            return $request;
        } elseif ( ! empty( $post ) ) {
            return $request;
        }

        return $request;
    }

    /**
     * Set current category
     *
     * @param object $wp_query
     * @return object
     */
    public function parse_query( $wp_query ) {
        if ( is_tax( $this->woocommerce_taxonomy ) ) {
            $this->current_category = get_term_by( 'slug', get_query_var( $this->woocommerce_taxonomy ), $this->woocommerce_taxonomy );
        }

        return $wp_query;
    }

    /**
     * Do redirect
     *
     * @param string $request
     * @return boolean
     */
    public function redirect( $url ) {
        global $wpdb;

        // Get redirect data
        $data = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}{$this->table_redirects} WHERE redirect_url = %s", $url ) );

        if ( ! is_null( $data ) ) {
            $redirect = null;
            switch ( $data->redirect_type ) {
                case 'post' :
                    if ( get_option( 'psu_redirect_product' ) === 'yes' ) {
                        $redirect = get_permalink( $data->redirect_entity_id );
                    }
                    break;
                case 'term' :
                    if ( get_option( 'psu_redirect_category' ) === 'yes' ) {
                        $redirect = get_term_link( (int)$data->redirect_entity_id, $this->woocommerce_taxonomy );
                    }
                    break;
                case 'tag' :
                    if ( get_option( 'psu_redirect_tag' ) === 'yes' && get_option( 'psu_tag_support' ) === 'yes' ) {
                        $redirect = get_term_link( (int)$data->redirect_entity_id, $this->woocommerce_tag_taxonomy );
                    }
                    break;
            }

            if ( $redirect === $url ) {
                return false;
            }

            // Do redirect
            if ( is_string( $redirect ) ) {
                wp_redirect( $redirect, $data->redirect_status );
                exit;
            }
        }

        return false;
    }

    /**
     * Set canonical
     */
    public function canonical( $canonical = '' ) {
        // Product category canonical
        if ( is_tax( $this->woocommerce_taxonomy ) ) {
            if ( did_action( 'wpseo_head' ) && get_option( 'psu_rewrite_yoast_canonical' ) === 'no' ) {
                return $canonical;
            }
            return $this->product_cat_canonical( $canonical );
        }

        // Product canonical
        if ( ! is_null( $this->current_post ) ) {
            if ( did_action( 'wpseo_head' ) && get_option( 'psu_rewrite_yoast_canonical' ) === 'no' ) {
                return $canonical;
            }
            return $this->product_canonical( $canonical );
        }

        // Default canonical
        if ( is_singular() ) {
            if ( did_action( 'wpseo_head' ) ) {
                return $canonical;
            }
            return $this->post_canonical();
        }

        return $canonical;
    }

    /**
     * Set product category canonical
     *
     * @global WP_Query $wp_the_query
     */
    public function product_cat_canonical( $canonical ) {
        global $wp_the_query;
        if ( ! $id = $wp_the_query->get_queried_object_id() )
            return;

        $link = get_term_link( $id, $this->woocommerce_taxonomy );

        if ( $page = get_query_var('paged') )
            $link = apply_filters( 'psu_product_cat_canonical_page_link', get_pagenum_link( $page ), $page, $id );

        /**
         * Filter: 'psu_product_cat_canonical_after' - Allow filtering of canonical url
         */
        $link = apply_filters( 'psu_product_cat_canonical_after', $link, $id );

        if ( ! empty( $canonical ) ) {
            return $link;
        }

        echo "<link rel='canonical' href='$link' />\n";
    }

    /**
     * Set product canonical
     *
     * @global WP_Query $wp_the_query
     */
    public function product_canonical( $canonical ) {
        $link = get_permalink( $this->current_post->ID );

        // @TODO: Need to rethink this
        if ( get_option( 'psu_product_canonical_nc' ) === 'yes' ) {
            $woocommerce_permalinks = get_option( 'woocommerce_permalinks' );

            // @TODO: Refactor this. Create some global/general function to strip the product base so everywhere the base url will be stripped the same way.
            $find = array(
                '/' . $this->woocommerce_product_slug . '/',
                '/product/',
                '/product',
                '%product_cat%'
            );

            $replace = array(
                '/',
                '/',
                '',
                ''
            );

            $product_base = str_replace( $find, $replace, $woocommerce_permalinks['product_base'] );

            /**
             * Filter: 'psu_product_canonical_base_after' - Allow filtering of product base
             */
            $product_base = apply_filters( 'psu_product_canonical_base_after', $product_base, $this->current_post->ID );
            $product = get_post( $this->current_post->ID );

            // Add or remove slash at end of the url
            // @TODO: check 'user_trailingslashit'
            $slash = '';
            if ( substr( $link, -1 ) === '/' ) {
                $slash = '/';
            }

            $link = home_url( trailingslashit( $product_base ) . $product->post_name . $slash );
        }

        /**
         * Filter: 'psu_product_canonical_after' - Allow filtering of canonical url
         */
        $link = apply_filters( 'psu_product_canonical_after', $link, $this->current_post->ID );

        if ( ! empty( $canonical ) ) {
            return $link;
        }

        echo "<link rel='canonical' href='$link' />\n";
    }

    /**
     * Set post canonical
     *
     * @global WP_Query $wp_the_query
    */
    public function post_canonical() {
        global $wp_the_query;
        if ( !$id = $wp_the_query->get_queried_object_id() )
            return;

        $link = get_permalink( $id );

        if ( $page = get_query_var('cpage') )
            $link = get_comments_pagenum_link( $page );

        /**
         * Filter: 'psu_post_canonical_after' - Allow filtering of canonical url
         */
        $link = apply_filters( 'psu_post_canonical_after', $link, $id );

        echo "<link rel='canonical' href='$link' />\n";
    }

    /**
     * Fix url in sitemap when psu_product_canonical_nc = yes
     *
     * @param string $url
     * @param object $post
     *
     * @return string
     */
    public function wpseo_xml_sitemap_post_url( $url, $post ) {
        if ( $post->post_type !== $this->woocommerce_post_type ) {
            return $url;
        }

        $woocommerce_permalinks = get_option( 'woocommerce_permalinks' );

        $find = array(
            '/' . $this->woocommerce_product_slug . '/',
            '/product/',
            '/product',
            '%product_cat%'
        );

        $replace = array(
            '/',
            '/',
            '',
            ''
        );

        $product_base = str_replace( $find, $replace, $woocommerce_permalinks['product_base'] );

        /**
         * Filter: 'psu_product_canonical_base_after' - Allow filtering of product base
         */
        // @TODO: Maybe this filter needs to be renamed.
        $product_base = apply_filters( 'psu_product_canonical_base_after', $product_base );

        // Add or remove slash at end of the url
        $slash = '';
        if ( substr( $url, -1 ) === '/' ) {
            $slash = '/';
        }

        $url = home_url( trailingslashit( $product_base ) . $post->post_name . $slash );

        /**
         * Filter: 'psu_xml_sitemap_post_url_after' - Allow filtering of url
         */
        $url = apply_filters( 'psu_xml_sitemap_post_url_after', $url );

        return $url;
    }

    /**
     * Is plugin active
     *
     * @return boolean
     */
    public function is_activated( $check_install = false ) {
        if ( $check_install && ! $this->is_installed() ) {
            return false;
        }

        if ( get_option( 'psu_activated' ) === 'Activated' ) {
            return true;
        }
        return false;
    }

    /**
     * Is plugin installed
     *
     * @return boolean
     */
    public function is_installed() {
        if ( get_option( 'psu_install_complete' ) === 'yes' ) {
            return true;
        }
        return false;
    }

    /**
     * Ajax request handler
     */
    public static function ajax() {
        if ( is_admin() ) {
            PSU_Admin::ajax();
        }
    }

    public function get_current_category() {
        return $this->current_category;
    }

}

add_action( 'wp_ajax_psu_ajax', array( PSU(), 'ajax' ) );

endif;

/**
 * Returns the main instance of PSU to prevent the need to use globals.
 *
 * @since  2.2.1
 * @return PSU
 */
function PSU() {
    return PSU::instance();
}

// Global for backwards compatibility.
$GLOBALS['psu'] = PSU();