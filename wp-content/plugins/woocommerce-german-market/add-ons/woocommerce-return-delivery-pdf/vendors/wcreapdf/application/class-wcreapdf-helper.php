<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WCREAPDF_Helper' ) ) {
	
	/**
	* some functions that helps handling temp files, option names and if an order needs the pdf as attachment
	*
	* @class WCREAPDF_Helper
	* @version 1.0
	* @category	Class
	*/
	class WCREAPDF_Helper {	
		
		/**
		* get option value by my option name
		*
		* @since 0.0.1
		* @access public
		* @arguments string $option (my option name)	
		* @return mixed: boolean false or string
		*/	
		public static function get_wcreapdf_optionname( $option ) {
			if ( $option ) {
				return 'woocomerce_wcreapdf_wgm_' . sanitize_title( $option );		
			} else {
				return false;	
			}
		}
		
		/**
		* checks whether $order needs the retoure pdf (equiv. to needs shipping)
		*
		* @since 0.0.1
		* @access public
		* @arguments WC_Order $order
		* @return boolean
		*/	
		public static function check_if_needs_attachement( $order ) {
			
			if ( ! is_a( $order, 'WC_Order' ) ) {
				return $order;
			}
			
			$items = $order->get_items();
			foreach ( $items as $item_id => $item ) {
				
				$_product		= $order->get_product_from_item( $item );
				
				if( ! method_exists( $_product, 'needs_shipping' ) ) { // some items aren't products (probably romoved from shop)
					continue;
				}
				
				if ( $_product->needs_shipping() ) {
					return true;	
				}
				
			}
			return false;
		}
	} // end class
	
} // end if
