<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WCREAPDF_Backend_Download' ) ) {
	
	/**
	* enables download buttons in backend
	*
	* @class WCREAPDF_Backend_Download
	* @version 1.0
	* @category	Class
	*/
	class WCREAPDF_Backend_Download {
		
		/**
		* adds 'create retoure pdf' to order actions options
		*
		* @since 0.0.1
		* @access public
		* @static
		* @hook woocommerce_order_actions
		* @arguments $array
		* @return $array ($actions => $optionname)
		*/	
		public static function order_action_option( $array ) {
			$array[ 'woocomerce_wcreapdf_wgm_sendretoure' ] = __( 'Download retoure pdf', 'woocommerce-german-market' );
			return $array;
		}

		/**
		* adds 'create delivery pdf' to order actions options
		*
		* @since GM v3.2
		* @access public
		* @static
		* @hook woocommerce_order_actions
		* @arguments $array
		* @return $array ($actions => $optionname)
		*/	
		public static function order_action_option_delivery( $array ) {
			$array[ 'woocomerce_wcreapdf_wgm_senddelivery' ] = __( 'Download delivery pdf', 'woocommerce-german-market' );
			return $array;
		}
		
		/**
		* create the retoure pdf to shop user when choosing this option and force download
		*
		* @since 0.0.1
		* @access public
		* @static
		* @hook woocommerce_order_action_woocomerce_wcreapdf_sendretoure
		* @arguments $order
		* @return void
		*/		
		public static function order_action( $order ) {
			if ( WCREAPDF_Helper::check_if_needs_attachement( $order ) ) {	
				WCREAPDF_Pdf::create_pdf( $order, false, true );
			}
		}

		/**
		* create the delivery pdf to shop user when choosing this option and force download
		*
		* @since GM v3.2
		* @access public
		* @static
		* @arguments $order
		* @return void
		*/		
		public static function order_action_delivery( $order ) {
			if ( WCREAPDF_Helper::check_if_needs_attachement( $order ) ) {	
				WCREAPDF_Pdf_Delivery::create_pdf( $order, false, true );
			}
		}
		
		/**
		* adds a small download button to the admin page for orders
		*
		* @since 0.0.1
		* @access public
		* @static 
		* @hook woocommerce_admin_order_actions
		* @arguments $actions, $theOrder
		* @return $actions
		*/	
		public static function admin_icon_download( $actions, $order ) {
			if ( WCREAPDF_Helper::check_if_needs_attachement( $order ) ) {
				$create_pdf = array( 
								'url' 		=>	wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_wcreapdf_download&order_id=' . $order->get_id() ), 'woocommerce-wcreapdf-download' ), 
								// would be nice do add html5 attribute download
								// so you get in chrome: Resource interpreted as Document but transferred with MIME type application
								'name' 		=> __( 'Download retoure pdf', 'woocommerce-german-market' ),
								'action' 	=> "retoure"
							);
				$actions[ 'retoure' ]	= $create_pdf;	
			}
			return $actions;
		}

		/**
		* adds a small download button for delivery pdf to the admin page for orders
		*
		* @since GM v3.2
		* @access public
		* @static 
		* @hook woocommerce_admin_order_actions
		* @arguments $actions, $theOrder
		* @return $actions
		*/	
		public static function admin_icon_download_delivery( $actions, $order ) {
			if ( WCREAPDF_Helper::check_if_needs_attachement( $order ) ) {
				$create_pdf = array( 
								'url' 		=>	wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_wcreapdf_download_delivery&order_id=' . $order->get_id() ), 'woocommerce-wcreapdf-download' ), 
								// would be nice do add html5 attribute download
								// so you get in chrome: Resource interpreted as Document but transferred with MIME type application
								'name' 		=> __( 'Download delivery pdf', 'woocommerce-german-market' ),
								'action' 	=> "delivery_pdf"
							);
				$actions[ 'delivery' ]	= $create_pdf;	
			}
			return $actions;
		}
		
		/**
		* ajax, manages what happen when the downloadbutton on admin order page is clicked
		*
		* @since 0.0.1
		* @access public
		* @static 
		* @hook wp_ajax_woocommerce_wcreapdf_download
		* @arguments $_REQUEST[ 'order_id' ]
		* @return void, exit()
		*/	
		public static function admin_ajax_download_pdf() {
			check_ajax_referer( 'woocommerce-wcreapdf-download', 'security' );
			$order_id	= $_REQUEST[ 'order_id' ];
			$order 		= new WC_Order( $order_id );
			self::order_action( $order );
			exit();
		}

		/**
		* ajax, manages what happen when the downloadbutton on admin order page is clicked
		*
		* @since GM v3.2
		* @access public
		* @static 
		* @hook wp_ajax_woocommerce_wcreapdf_download
		* @arguments $_REQUEST[ 'order_id' ]
		* @return void, exit()
		*/	
		public static function admin_ajax_download_pdf_delivery() {
			check_ajax_referer( 'woocommerce-wcreapdf-download', 'security' );
			$order_id	= $_REQUEST[ 'order_id' ];
			$order 		= new WC_Order( $order_id );
			self::order_action_delivery( $order );
			exit();
		}
		
		/**
		* ajax, manages test pdf download
		*
		* @since 0.0.1
		* @access public
		* @static 
		* @hook wp_ajax_woocommerce_wcreapdf_download_test_pdf
		* @return void, exit()
		*/	
		public static function download_test_pdf() {
			check_ajax_referer( 'woocommerce-wcreapdf-download-test-pdf', 'security' );
			WCREAPDF_Pdf::create_pdf( NULL, true, 'D' );
			exit();
		}

		/**
		* ajax, manages test pdf download PDF Delivery
		*
		* @since GM v3.2
		* @access public
		* @static 
		* @hook wp_ajax_woocommerce_wcreapdf_download_test_pdf_delivery
		* @return void, exit()
		*/	
		public static function download_test_pdf_delivery() {
			check_ajax_referer( 'woocommerce-wcreapdf-download-test-pdf', 'security' );
			WCREAPDF_Pdf_Delivery::create_pdf( NULL, true, 'D' );
			exit();
		}
	} // end class
} // end if
?>