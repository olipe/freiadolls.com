<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} 

/**
* Add Tabs to Product Page
*
* @wp-hook woocommerce_product_tabs
* @param Array $tabs
* @return Array
**/
function gm_fic_product_tab( $tabs ) {

	// Do we have to add the tab for nutritional_values?
	$terms = get_terms( 'gm_fic_nutritional_values', array( 'orderby' => 'slug', 'hide_empty' => 0 ) );
	$add_tab = false;
	global $product;
	$id = $product->get_id();

	foreach ( $terms as $term ) {

		$post_meta = get_post_meta( $id, '_nutritional_values_' . $term->slug, true );
		if ( $post_meta != '' ) {
			$add_tab = true;
			break;
		}

	}

	// add tab nutritional_values
	if ( $add_tab ) {
		
		$tabs[ 'gm_fic_nutritional_values' ] = array(
			'title' 	=> get_option( 'gm_fic_ui_frontend_labels_nutritional_values', __( 'Nutritional Values', 'woocommerce-german-market' ) ),
			'priority' 	=> 25,
			'callback' 	=> 'gm_fic_tab_content_nutritional_values'
		);

	}
	
	// Do we have to add the tab for allergens?
	$allergens_info = get_post_meta( $id, '_allergens_info', true );
	if ( $allergens_info != '' ) {
		
		$tabs[ 'gm_fic_allergens' ] = array(
			'title' 	=> get_option( 'gm_fic_ui_frontend_labels_allergens', __( 'Allergens', 'woocommerce-german-market' ) ),
			'priority' 	=> 26,
			'callback' 	=> 'gm_fic_tab_content_allergens'
		);
		
	}

	return $tabs;
}

/**
* Render Tab for Nutritional Values
*
* @return void
**/
function gm_fic_tab_content_nutritional_values() {
	
	global $product;
	$id = $product->get_id();
	?>
	<h2><?php echo get_option( 'gm_fic_ui_frontend_labels_nutritional_values', __( 'Nutritional Values', 'woocommerce-german-market' ) ); ?></h2>

	<div id="gm_fic_nutritional_values">
		<?php gm_fic_tab_content_nutritional_values_by_id( $id ); ?>
	</div>

	<?php

}

/**
* Render Tab for Allergens
*
* @return void
**/
function gm_fic_tab_content_allergens() {
	
	global $product;
	$id = $product->get_id();
	
	?>
	<h2><?php echo get_option( 'gm_fic_ui_frontend_labels_allergens', __( 'Allergens', 'woocommerce-german-market' ) ); ?></h2>

	<div id="gm_fic_allergens">
		<?php gm_fic_tab_content_allergens_by_id( $id ); ?>
	</div>

	<?php
}

/**
* Get Nutritional Values Table(ajax and none-ajax)
*
* @param Integer $id
* @return void
**/
function gm_fic_tab_content_nutritional_values_by_id( $id ) {

	?>
	<span class="gm_fic_nutritional_values_remark"><?php echo get_post_meta( $id, '_nutritional_values_remark', true ); ?></span>

		<?php
		
		$terms 					= get_terms( 'gm_fic_nutritional_values', array( 'orderby' => 'slug', 'hide_empty' => 0 ) );
		$default_nutritionals 	= gm_fic_get_default_nutritionals();
		$prefix 				= get_option( 'gm_fic_ui_frontend_prefix_nutritional_values', __( '- of which', 'woocommerce-german-market' ) );
		if ( $prefix != '' ) {
			$prefix = ' ' . $prefix . ' ';
		}

		?><table class="gm-fic-nutritional-values"><?php

		$terms = apply_filters( 'gm_fic_order_terms_frontend', $terms );
		
		foreach ( $terms as $term ) {

			$post_key		= '_nutritional_values_' . $term->slug;
			$post_value		= get_post_meta( $id, $post_key, true );

			if ( $post_value == '' ) {

				// if post_value is empty and it is a variation => try to get value from parent product
				$product = wc_get_product( $id );
				if ( is_a( $product, 'WC_Product_Variation' ) ) {
					$post_value = get_post_meta( $product->get_parent_id(), $post_key, true );
				}
			}

			// if value is empty and this field is not required => continue
			$required = false;
			if ( isset( $default_nutritionals[ $term->slug ] ) ) {
				$required = isset( $default_nutritionals[ $term->slug ][ 'required' ] ) && $default_nutritionals[ $term->slug ][ 'required' ];
			}

			if ( ! $required && $post_value == '' ) {
				continue;
			}

			$label = $term->name;

			// prefix if term has parent
			if ( isset( $default_nutritionals[ $term->slug ] ) ) {
				$has_parent = isset( $default_nutritionals[ $term->slug ][ 'parent' ] ) && $default_nutritionals[ $term->slug ][ 'parent' ];
				if ( $has_parent ) {
					$label = $prefix . $label;
				}
			} else {
				if ( isset( $term->parent ) && $term->parent > 0 ) {
					$label = $prefix . $label;
				}
			}

			?>
			<tr class="gm-fic-nutritional-values-tr">
				<td class="gm-fic-nutritional-values-td gm-fic-nutritional-values-td-label"><?php echo $label; ?></td>
				<td class="gm-fic-nutritional-values-td gm-fic-nutritional-values-td-value"><?php echo $post_value; ?></td>
				<?php do_action( 'gm_fic_nutritional_values_after_term_in_tr', $id, $term ); ?>
			</tr>
			<?php

			do_action( 'gm_fic_nutritional_values_after_tr', $id );

		}

		?></table><?php

		do_action( 'gm_fic_nutritional_values_after_table', $id );

}

/**
* Get Allergens Table(ajax and none-ajax)
*
* @param Integer $id
* @return void
**/
function gm_fic_tab_content_allergens_by_id( $id ) {
	
	$allergens_info = get_post_meta( $id, '_allergens_info', true );
	
	if ( $allergens_info == '' ) {

		// if allergens_info is empty and it is a variation => try to get value from parent product
		$product = wc_get_product( $id );
		if ( is_a( $product, 'WC_Product_Variation' ) ) {
			$allergens_info = get_post_meta( $product->get_parent_id(), '_allergens_info', true );
		}
	}
			
	echo apply_filters( 'the_content', $allergens_info );
}

/**
* Register JS
*
* @wp-hook wp_enqueue_scripts
* @return void
**/
function gm_fic_product_tab_scripts() {
	
	$min = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : 'min.';
	
	wp_register_script( 'gm-fic-frontend', untrailingslashit( plugin_dir_url( __FILE__ ) ) . '/assets/js/frontend.' . $min . 'js', array( 'jquery' ) );
	wp_enqueue_script( 'gm-fic-frontend' );
	wp_localize_script( 'gm-fic-frontend', 'gm_fix_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'nonce' => wp_create_nonce( 'gm-fic-frontend' ) ) );

}
