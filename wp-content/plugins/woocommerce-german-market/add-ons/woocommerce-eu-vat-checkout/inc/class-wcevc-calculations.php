<?php

/**
 * Class WCEVC_Calculations
 */
class WCEVC_Calculations {

	/**
	 * @var null|WCEVC_Calculations
	 */
	private static $instance = NULL;

	/**
	 * Private clone method to prevent cloning of the instance of the
	 * *Singleton* instance.
	 *
	 * @return void
	 */
	private function __clone() { }

	/**
	 * @return WCEVC_Calculations
	 */
	public static function get_instance() {

		if ( self::$instance === NULL ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * @return  WCEVC_Calculations
	 */
	private function __construct() { }

	/**
	 * Callback to calculate correct price for downloadable products for "billing address tax rates" !== "shop base tax rates".
	 *
	 * @wp-hook woocommerce_get_price_including_tax
	 *
	 * @param   int $price
	 * @param   int $qty
	 * @param   WC_Product $product
	 *
	 * @return  int $price
	 */
	public function get_price_for_downloadable_products( $price, $qty, $product ) {
		
		if ( WGM_Session::get( 'eu_vatin_check_exempt' ) ) {
			return $price;
		}

		$price_include_taxes = get_option( 'woocommerce_prices_include_tax' );
		if ( $price_include_taxes === 'no' ) {
			return $price;
		}

		if ( $this->is_product_vatmoss_eligible( $product ) ) {
			$price = $product->get_price() * $qty;
		}

		return $price;

	}


	/**
	 * Recalculate totals and overwrite cart totals data.
	 *
	 * @wp-hook woocommerce_cart_item_subtotal, woocommerce_after_calculate_totals
	 *
	 * @param   WC_Cart $cart
	 *
	 * @return  void
	 */
	public function recalculate_cart( WC_Cart $cart ) {

		// if prices are excluded tax, we got nothing to do here.
		if ( ! $cart->prices_include_tax ) {
			return;
		}

		if ( WGM_Session::get( 'eu_vatin_check_exempt' ) ) {
			return;
		}

		$cart_items = $cart->get_cart();

		// if the cart has no vatmoss-products, we got - again - nothing to do here.
		if ( ! $this->has_vatmoss_products( $cart_items ) ) {
			return;
		}

		// reset coupon_discount_amounts
		$cart->coupon_discount_amounts = array();
		$cart->coupon_discount_tax_amounts = array();

		// reset tax rates
		$cart->taxes = array();

		foreach ( $cart_items as $cart_item_key => $line_item ) {

			$product = $line_item[ 'data' ];
			
			// generate new item regarless if it is a vatmpss-product or not
			// we need the included calculations of discounts and taxes of the $cart
			$new_line_item = $this->generate_new_item( $line_item, $cart );

			// if the product is not a vatmoss-product -> nothing to do.
			if ( $this->is_product_vatmoss_eligible( $product ) ) {

				// set new subtotal (brutto)
				$cart->subtotal -= $line_item[ 'line_subtotal' ] + $line_item[ 'line_subtotal_tax' ];
				$cart->subtotal += $new_line_item[ 'line_subtotal' ] + $new_line_item[ 'line_subtotal_tax' ];

				// set new subtotal excluding tax (netto)
				$cart->subtotal_ex_tax -= $line_item[ 'line_subtotal' ];
				$cart->subtotal_ex_tax += $new_line_item[ 'line_subtotal' ];

				// set new cart_contents_total (netto - discount)
				$cart->cart_contents_total -= $line_item[ 'line_total' ];
				$cart->cart_contents_total += $new_line_item[ 'line_total' ];

				// set new cart total
				$cart->total -= $line_item[ 'line_total' ] + $line_item[ 'line_tax' ];
				$cart->total += $new_line_item[ 'line_total' ] + $new_line_item[ 'line_tax' ];

				// set new total tax
				$cart->tax_total -= $line_item[ 'line_tax' ];
				$cart->tax_total += $new_line_item[ 'line_tax' ];

				// set new discount amount
				// remove old line discount
				$cart->discount_cart -= $line_item[ 'line_subtotal' ] - $line_item[ 'line_total' ];
				// do not add new discount of line_item! because it's already done in calculations 

				// set new discount tax amount
				$cart->discount_cart_tax -= $line_item[ 'line_subtotal_tax' ] - $line_item[ 'line_tax' ];
				$cart->discount_cart_tax += $new_line_item[ 'line_subtotal_tax' ] - $new_line_item[ 'line_subtotal_tax' ];
				
				// overwrite the old item with the new one.
				$cart->cart_contents[ $cart_item_key ] = $new_line_item;

			}

		}

		$cart->set_session();
	}

	/**
	 * Returns if the given cart_items contain at least one product for vatmoss-calcualations.
	 *
	 * @param array $cart_items		the cart-items of $cart->get_cart().
	 *
	 * @return boolean true|false   true, if the cart has at least one vatmoss-product, false if no vatmoss-product was found.
	 */
	public function has_vatmoss_products( $cart_items ) {
		$has_vatmoss_products = false;

		foreach ( $cart_items as $item ) {
			$product = $item[ 'data' ];
			if ( $this->is_product_vatmoss_eligible( $product ) ) {
				$has_vatmoss_products = true;
				break;
			}
		}

		return $has_vatmoss_products;
	}

	/**
	 * Generate the new line item with calculated prices and taxes based on brutto-price.
	 *
	 * @param   array $item
	 * @param   WC_Cart $cart
	 *
	 * @return  array $line_data    the new $item with correct prices
	 */
	private function generate_new_item( $item, WC_Cart $cart ) {

		if ( WGM_Session::get( 'eu_vatin_check_exempt' ) ) {
			return $item;
		}

		/** @var \WC_Product $product */
		$product	= $item[ 'data' ];
		$quantity	= $item[ 'quantity' ];

		// this is important for not-vatmoss-products to calculate correct discount prices and taxes
        $base_price = ( get_option( 'woocommerce_tax_display_cart' ) == 'excl' ) ? wc_get_price_excluding_tax( $product ) : wc_get_price_including_tax( $product );

		// find the correct tax rates for calculation
		if ( is_user_logged_in() ) {
			
			if ( WC()->customer && ( ! is_null( WC()->customer ) ) ) {

				$args = array(
					'tax_class' => $product->get_tax_class(),
					'country'   => WC()->customer->get_billing_country()
				);
				$tax_rates = WC_Tax::find_rates( $args );
			
			} else {

				$tax_rates = WC_Tax::get_rates( $product->get_tax_class() );

			}

			
		} else {
			$tax_rates = WC_Tax::get_rates( $product->get_tax_class() );
		}

		// Calculate product price and tax (discounted)
		$recalculate_totals	   = $this->is_product_vatmoss_eligible( $product );
		$discounted_price      = $cart->get_discounted_price( $item, $base_price, TRUE );
		$discounted_taxes      = WC_Tax::calc_tax( $discounted_price * $quantity, $tax_rates, TRUE );
		$discounted_tax_amount = array_sum( $discounted_taxes );
		$line_tax              = $discounted_tax_amount;
		$line_total            = $discounted_price * $quantity;

		// Set cart taxes
		foreach( $discounted_taxes as $discounted_tax_key => $discounted_tax ) {
			if ( isset( $cart->taxes[ $discounted_tax_key ] ) ) {
				$cart->taxes[ $discounted_tax_key ] += $discounted_tax;
			} else {
				$cart->taxes[ $discounted_tax_key ] = $discounted_tax;
			}
		}

		if ( $recalculate_totals ) {

			$line_subtotal                  = $base_price * $quantity;
			$calculated_line_subtotal_tax   = WC_Tax::calc_tax( $line_subtotal, $tax_rates, TRUE );
			$line_subtotal_tax              = array_sum( $calculated_line_subtotal_tax );

			// overwrite the prices from item
			$item[ 'line_total' ]       = $line_total - $line_tax;             // netto
			$item[ 'line_tax' ]         = $line_tax;
			$item[ 'line_subtotal' ]    = $line_subtotal - $line_subtotal_tax; // netto
			$item[ 'line_subtotal_tax' ]= $line_subtotal_tax;
			$item[ 'line_tax_data' ]    = array(
				'total'		=> $discounted_taxes,
				'subtotal'	=> $calculated_line_subtotal_tax
			);
       
		}

		return $item;
	}

	/**
	 *  Check if ...
	 *      - the product is downloadable
	 *      - the selected customer country is inside the EU.
	 *      - the "shop base tax rate" !== "product tax rate"
	 *
	 * This method requires the option 'wcevc_enabled_wgm' set to 'downloadable'.
	 *
	 * @param WC_Product $product
	 *
	 * @return bool
	 */
	public static function is_product_vatmoss_eligible( $product ) {

		if ( ! $product->is_downloadable() ) {
			return false;
		}

		$wc = WC();
		if ( empty( $wc->customer ) ) {
			return false;
		}

		$eu_countries       = $wc->countries->get_european_union_countries();
		$customer_country   = $wc->customer->get_billing_country();
		if ( ! in_array( $customer_country, $eu_countries ) ) {
			return false;
		}

		$tax_rates           = WC_Tax::get_rates( $product->get_tax_class() );
		$shop_base_tax_rates = WC_Tax::get_base_tax_rates( $product->get_tax_class() );
		if ( $tax_rates === $shop_base_tax_rates ) {
			return false;
		}

		return true;
	}

}