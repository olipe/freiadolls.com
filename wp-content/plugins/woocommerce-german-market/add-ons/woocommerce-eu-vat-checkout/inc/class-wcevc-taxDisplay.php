<?php

/**
 * Class WCEVC_TaxDisplay
 */
class WCEVC_TaxDisplay {

	/**
	 * @var null|WCEVC_TaxDisplay
	 */
	private static $instance = NULL;

	/**
	 * Private clone method to prevent cloning of the instance of the
	 * *Singleton* instance.
	 *
	 * @return void
	 */
	private function __clone() {
	}

	/**
	 * @return WCEVC_TaxDisplay
	 */
	public static function get_instance() {

		if ( self::$instance === NULL ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * @return  WCEVC_TaxDisplay
	 */
	private function __construct() {
	}

	/**
	 * Returns the tax-string without tax-rate within archive- and single-template.
	 *
	 * @wp-hook wgm_tax_text
	 *
	 * @param   string     $text
	 * @param   WC_Product $product
	 * @param   string     $include_string
	 * @param   array      $rate
	 *
	 * @return  string $text
	 */
	public function print_tax_string_without_tax_rate( $text, WC_Product $product, $include_string, $rate ) {

		if ( ! $product->is_downloadable() ) {
			return $text;
		}

		// We're using the WC-translation to support as much as possible languages by default.
		$text = sprintf(
		/* translators: %1$s: tax rate label */
			__( '(includes %s)', 'woocommerce' ),
			$rate[ 'label' ]
		);

		// replace the ( and ) from WC-translation.
		$search = array(
			'(',
			')'
		);

		return str_replace( $search, '', $text );
	}

}