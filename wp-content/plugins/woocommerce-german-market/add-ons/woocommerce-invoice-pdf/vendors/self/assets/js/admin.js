jQuery( document ).ready( function( $ ) {

    $( '#wp_wc_invoice_pdf_image_upload_button_footer, #wp_wc_invoice_pdf_image_upload_button_header, #wp_wc_invoice_pdf_image_upload_button_background' ).click( function() {

		var this_id 		= ( $(this).attr( 'id' ) );
		var formfield_id = this_id.replace( 'wp_wc_invoice_pdf_image_upload_button_', 'wp_wc_invoice_pdf_image_url_' );
		
		window.send_to_editor = function( html ) {
			imgurl = $( 'img', html ).attr( 'src' );
			if ( typeof imgurl == "undefined" ) {
				imgurl = $( html ).attr( 'src' );
			}
			$( '#' + formfield_id ).val( imgurl );
			tb_remove();
	    }
		
		tb_show( '', 'media-upload.php?type=image&amp;TB_iframe=true' );
        return false;
		
    });
	
	$( '#wp_wc_invoice_pdf_image_remove_button_header, #wp_wc_invoice_pdf_image_remove_button_footer, #wp_wc_invoice_pdf_image_remove_button_background' ).click( function() {
		var this_id 		= ( $(this).attr( 'id' ) );
		var formfield_id = this_id.replace( 'wp_wc_invoice_pdf_image_remove_button_', 'wp_wc_invoice_pdf_image_url_' );
		$( '#' + formfield_id ).val( '' );
    });

    $( '.refund_pdf' ).click( function() {
    	var refund_id = $( this ).attr( 'data-refund-id' );
    	var delete_button = $( this ).next( "[data-refund-id='" + refund_id + "']" ).show();
    });

    $( '.invoice_pdf' ).click( function() {
    	if (  ! $( this ).hasClass( 'always_create_new' ) ) {
    		$( this ).next( '.invoice_pdf_delete_content' ).show();
    	}
    });

    $( '.gm-select-all-refunds' ).click( function() {
    	var checked = $( this ).is( ":checked" );
    	$( '.gm-select-refund, .gm-select-all-refunds' ).prop( 'checked', checked ); 
    });

});
