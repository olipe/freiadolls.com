<?php
/**
 * Template for invoice content
 *
 * Override this template by copying it to yourtheme/woocommerce-invoice-pdf/revocation-policy.php
 *
 * @version     3.0.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} 
// default styles from plugin
$color						= get_option( 'wp_wc_invoice_pdf_additional_pdf_text_color', '#000000' );
$font						= get_option( 'wp_wc_invoice_pdf_additional_pdf_font', 'Helvetica' );
$font_size					= get_option( 'wp_wc_invoice_pdf_additional_pdf_font_size', 10 );
?>
	<style>
		p,  time, table, tr, th, td, span, h1, h2, h3, h4, h5, h6,  {
			font-family: <?php echo $font; ?>;
			line-height: normal;
			vertical-align: middle;	
			color: <?php echo $color; ?>;
		}

		h1{ font-size: 12pt; }
		h2, h3, h4, h5, h6 { font-size: 11pt; }

		p{
			font-family: <?php echo $font; ?>;
			font-size: <?php echo $font_size - 2; ?>pt;
			margin: 0.75em 0;
			text-align: justify;
		}

		ul li{
			font-family: <?php echo $font; ?>;
			font-size: <?php echo $font_size - 2; ?>pt;
		}
		
	</style>
<?php

$pages = array(
	'wp_wc_invoice_pdf_additional_pdf_recovation_policy_page' 			=> get_page( get_option( 'woocommerce_widerruf_page_id') ),
	'wp_wc_invoice_pdf_additional_pdf_recovation_policy_digital_page' 	=> get_page( get_option( 'woocommerce_widerruf_fuer_digitale_medien_page_id' ) ),
);

foreach ( $pages as $option => $page ) {

	// Don't print page if option is not set
	if ( get_option( $option ) != 'yes' ) {
		continue;
	}

	?><h1><?php echo $page->post_title; ?></h1><?php

	echo apply_filters( 'the_content', WGM_Template::remove_vc_shortcodes( $page->post_content ) );

}
