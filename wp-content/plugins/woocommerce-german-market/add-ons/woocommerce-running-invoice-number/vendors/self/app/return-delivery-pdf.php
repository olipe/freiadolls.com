<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WP_WC_Running_Invoice_Number_Return_Delivery_Pdf' ) ) {
	
	/**
	* Return Delivery PDF: Replace Small Headline
	*
	* @WP_WC_Running_Invoice_Number_Invoice_Pdf
	* @version 1.0
	* @category	Class
	*/
	class WP_WC_Running_Invoice_Number_Return_Delivery_Pdf {	

		/**
		* Replace Small Headline - Backend Placeholders
		*
		* @wp-hook wcreapdf_pdf_placeholders_backend_string
		* @param String $string
		* @return $string
		**/
		public static function wcreapdf_pdf_placeholders_backend_string( $string ) {
			return $string . ', Invoice Number: <code>{{invoice-number}}</code>, Invoice Date: <code>{{invoice-date}}</code>';
		}

		/**
		* Replace Small Headline - Frontend Replace
		*
		* @wp-hook wcreapdf_pdf_placeholders_frontend_string
		* @param String $string
		* @return $string
		**/
		public static function wcreapdf_pdf_placeholders_frontend_string( $string, $order = NULL ) {

			$search = array( '{{invoice-number}}', '{{invoice-date}}' );

			if ( $order ) {
				$invoice_number = new WP_WC_Running_Invoice_Number_Functions( $order );
				$replace = array( $invoice_number->get_invoice_number(), $invoice_number->get_invoice_date() );
			} else {
				$replace = array( utf8_decode( rand( 1, 99 ) ), date_i18n( get_option( 'date_format' ), time() ) );
			}

			$string = str_replace( $search, $replace, $string );

			return $string;

		}
		
	} // end class
	
} // end if

