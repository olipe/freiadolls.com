<?php
/**
 * Feature Name: Variation Fields
 * Descriptions: These functions are adding the fields
 * Version:      1.0
 * Author:       MarketPress
 * Author URI:   https://marketpress.com
 * Licence:      GPLv3
 */

/**
 * Adds the field to the variations
 * 
 * @param	int $loop the current loop count
 * @param	array $variation_data the data of the current variation
 * @param	object $variation the variation post object
 * @return	void
 */
function wcppufv_add_field( $loop, $variation_data, $variation ) {

	?>
	<tr>
		<td colspan="2">

			<?php
			$smalltax = '<br /><small> ' . __( 'incl. Vat',  'woocommerce-german-market' ) . ' </small>';
			$regular_price_per_unit_selection = array( 'id' => 'variable_unit_regular_price_per_unit[' . $loop . ']', 'name' => '_v_unit_regular_price_per_unit' );

			$mult_field = '<span>&nbsp;&#47; &nbsp;</span> <input type="text" style="width: 40px;" name="variable_unit_regular_price_per_unit_mult[' . $loop . ']" value="'.  get_post_meta( $variation->ID, '_v_unit_regular_price_per_unit_mult', TRUE ) .'" />';

			// Price
			WGM_Settings::extended_woocommerce_text_input( array(
				'id'                             => 'variable_regular_price_per_unit[' . $loop . ']',
				'label'                          => __( 'Regular price',  'woocommerce-german-market' ) . ' (' . get_woocommerce_currency_symbol() . ')' . $smalltax,
				'value'                          => get_post_meta( $variation->ID, '_v_regular_price_per_unit', TRUE ),
				'between_input_and_desscription' => $mult_field . wcppufv_select_scale_units( $variation->ID, $regular_price_per_unit_selection ),
				'custom_attributes'				 => array( 'style' => 'width: 50%; max-width: 200px;' ) 
			) );

			$sale_price_per_unit_selection = array( 'id' => 'variable_unit_sale_price_per_unit[' . $loop . ']', 'name' => '_v_unit_sale_price_per_unit' );

			$mult_field = '<span>&nbsp;&#47; &nbsp;</span> <input type="text" style="width: 40px;" name="variable_unit_sale_price_per_unit_mult[' . $loop . ']" value="'.  get_post_meta( $variation->ID, '_v_unit_sale_price_per_unit_mult', TRUE ) .'" />';

			// Special Price
			WGM_Settings::extended_woocommerce_text_input( array(
				'id'                             => 'variable_sale_price_per_unit[' . $loop . ']',
				'label'                          => __( 'Sale Price',  'woocommerce-german-market' ) . ' (' . get_woocommerce_currency_symbol() . ')' . $smalltax ,
				'value'                          => get_post_meta( $variation->ID, '_v_sale_price_per_unit', TRUE ),
				'between_input_and_desscription' => $mult_field . wcppufv_select_scale_units( $variation->ID, $sale_price_per_unit_selection ),
				'custom_attributes'				 => array( 'style' => 'width: 50%; max-width: 200px;' ) 
			) );
			?>

		</td>
	</tr>
	<?php
}

/**
 * Adds the field to the variations
 * 
 * @param	int $post_id
 * @return	void
 */
function wcppufv_save_field( $post_id, $post = NULL ) {

	$simple = ( current_action() !== 'woocommerce_ajax_save_product_variations' );

		if ( ! empty( $_POST[ 'variable_post_id' ] ) ) {
			$variation_ids = $_POST[ 'variable_post_id' ];
		} else {
			$variation_ids = array();
		}

		/**
		 * meta_key => fallback_value
		 */
		$meta_keys = array(

				'variable_regular_price_per_unit'      		=> '',
				'variable_unit_regular_price_per_unit_mult' => '',
				'variable_unit_regular_price_per_unit'		=> '',

				'variable_sale_price_per_unit'      		=> '',
				'variable_unit_sale_price_per_unit_mult' 	=> '',
				'variable_unit_sale_price_per_unit'         => '',

		);

		foreach ( $variation_ids as $i => $post_id ) {

			foreach ( $meta_keys as $key => $fallback_value ) {
				
				if ( isset( $_POST[ $key ][ $i ] ) ) {
					$value = $_POST[ $key ][ $i ];
				} else{
					$value = '';
				}

				update_post_meta( $post_id, str_replace( 'variable', '_v', $key ), $value );
				
			}

		}

}
