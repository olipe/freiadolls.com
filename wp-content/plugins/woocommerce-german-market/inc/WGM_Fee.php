<?php

/**
 * Class WGM_Fee
 *
 * This class will fix the taxes for fee "Nachnahme" in "review-order"
 * and after creating the order
 *
 * @author  ChriCo
 */
class WGM_Fee {

	/**
	 * Calculating the split tax for fee on ajax callback in backend on "update tax"/"update sum"
	 *
	 * @wp-hook	woocommerce_saved_order_items
	 *
	 * @param	int $order_id
	 * @return	void
	 */
	public static function re_calculate_tax_on_save_order_items( $order_id ) {
        
        $use_split_tax = get_option( WGM_Helper::get_wgm_option( 'wgm_use_split_tax' ), 'on' );

        if ( $use_split_tax == 'off' ) {
            return;
        }

		$order = wc_get_order( $order_id );

		// getting all fees and remove them from order
		$all_fees = $order->get_fees();
		$order->remove_order_items( 'fee' );

		// loop through all shipping fees and create new ones with the split tax
		foreach ( $all_fees as $fee ) {

			if ( ! apply_filters( 'woocommerce_de_show_gateway_fees_tax', true, $fee ) ) {
				continue;
			}

			$bypass_digital = FALSE;
			//if ( $fee->id == WGM_Fee::get_cod_fee_id() )
			//	$bypass_digital = TRUE;

			if ( $fee && isset( $fee->id ) ) {
				$fee_id = $fee->id;
			} else {
				$fee_id = null;
			}

			$taxes = WGM_Tax::calculate_split_rate( $fee[ 'line_total' ], $order, $bypass_digital, $fee_id, 'fee' );

			// WC 3.0
			$new_fee            = new WC_Order_Item_Fee();

			$new_taxes = array();
			$new_taxes[ 'total' ] = array();

			foreach ( $taxes[ 'rates' ] as $tax_id => $tax ) {
				$new_taxes[ 'total' ][ $tax_id ] = $tax[ 'sum' ];
			}

			$new_fee->set_name( $fee->get_name() );
			$new_fee->set_tax_status( 'taxable' );
			$new_fee->set_tax_class( $fee->get_tax_class() );
			$new_fee->set_total( $fee->get_total() );
			$new_fee->set_total_tax( $taxes[ 'sum' ] );
			$new_fee->set_taxes( $new_taxes );

			$new_fee->apply_changes();

			// adding the new fee to order
			$order->add_item( $new_fee );
			$order->save();
		}

	}

	/**
	 * Adding Fee to gateway-Page second-checkout to display the taxes
	 *
	 * @wp-hook woocommerce_cart_calculate_fees
	 *
	 * @param   WC_Cart $cart
	 * @return  void
	 */
	public static function add_fee_to_gateway_page( WC_Cart $cart ) {
		
		$avail        = WC()->payment_gateways->get_available_payment_gateways();
		$chosen       = WC()->session->chosen_payment_method;
		$wgm_gateways = WGM_Gateways::get_gateway_fees();

		if ( isset( $wgm_gateways[ $chosen ], $avail[ $chosen ] ) ) {
			$g              = $avail[ $chosen ];
			$title          = __( $g->title, 'woocommerce-german-market' );
			$fee            = str_replace( ',', '.', $wgm_gateways[ $chosen ] );
			$tax_class_hack = 'wgm_' . $fee;

			$taxable        = ( get_option( 'woocommerce_tax_display_cart' ) === 'incl' );
			$cart->add_fee( $title, $fee, $taxable, $tax_class_hack );

		}
	}

	/**
	 * Adds the Fee with tax-string to review-order- and cart-totals-Template
	 *
	 * @wp-hook woocommerce_cart_totals_fee_html
	 *
	 * @param   string $fee_html
	 * @param   stdClass $fee
	 *
	 * @return  string $fee_html
	 */
	public static function show_gateway_fees_tax( $fee_html, $fee ) {

		if ( WGM_Tax::is_kur() ) {
			return $fee_html;
		}
		
		if ( ! apply_filters( 'woocommerce_de_show_gateway_fees_tax', true, $fee ) ) {
			return $fee_html;
		}

		$use_split_tax = get_option( WGM_Helper::get_wgm_option( 'wgm_use_split_tax' ), 'on' );

		if ( $use_split_tax == 'off' ) {

			// Setup.
			$tax             = self::get_highest_tax_rate();
			$calc_tax_result = WC_Tax::calc_tax( $fee->amount, $tax );
			$tax             = current( $tax );
			$tax_label       = $tax[ 'label' ];
			$tax_display        = get_option( 'woocommerce_tax_display_cart' );
			$tax_amount         = wc_price( current($calc_tax_result) );
			$tax_decimals       = WGM_Helper::get_decimal_length( $tax[ 'rate' ] );
			$tax_rate_formatted = number_format_i18n( (float) $tax[ 'rate' ], $tax_decimals );
			$tax_string         = WGM_Tax::get_excl_incl_tax_string( $tax_label, $tax_display, $tax_rate_formatted,
			                                                         $tax_amount );

			return $fee_html . sprintf(
				'<span class="wgm-tax product-tax"> %s </span>',
				$tax_string
			);
		}

		if ( ! empty( $fee->tax_class ) && substr( $fee->tax_class, 0, 4 ) == 'wgm_' ) {
			$amount = substr( $fee->tax_class, 4 );
		} else {
			$amount = $fee->amount;
		}

		$bypass_digital = FALSE;
		//if ( $fee->id == WGM_Fee::get_cod_fee_id() ) {
		//	$bypass_digital = TRUE;
		//}

		$rates = WGM_Tax::calculate_split_rate( $amount, WC()->cart, $bypass_digital, $fee->id, 'fee' );

		$fee_html .= WGM_Tax::get_split_tax_html( $rates, get_option( 'woocommerce_tax_display_cart' ) );

		return apply_filters( 'wgm_show_gateway_fees_tax', $fee_html, $fee );
	}
	/**
	 * Returns the highest tax rate of all cart items
	 *
	 * @return array
	 */
	public static function get_highest_tax_rate() {

		$cart         = WC()->cart->get_cart();
		$highest      = 0;
		$highest_rate = array();
		foreach ( $cart as $key => $item ) {

			// get the product
			$_product = apply_filters( 'woocommerce_cart_item_product', $item[ 'data' ], $item, $key );

			// get the product tax classes to set the array
			$class = $_product->get_tax_class();
			$rates = WC_Tax::get_rates( $class );

			foreach ( $rates as $rate_key => $rate ) {
				if ( $rate[ 'rate' ] > $highest ) {
					$highest = $rate[ 'rate' ];
					unset( $highest_rate );
					$highest_rate = array( $rate_key => $rate );
				}
			}

		}

		return $highest_rate;
	}

	/**
	 * Adding the correct split taxes to the fee-object.
	 *
	 * @author  ChriCo
	 *
	 * @wp-hook woocommerce_cart_calculate_fees
	 *
	 * @param   WC_Cart $cart   copied reference of the cart to manipulate the fee-tax
	 * @return  void
	 */
	public static function add_taxes_to_fee( WC_Cart $cart ) {

		if ( WGM_Tax::is_kur() ) {
			return;
		}

		$use_split_tax = get_option( WGM_Helper::get_wgm_option( 'wgm_use_split_tax' ), 'on' ) === 'on';

		// support for 3rd party plugins - check if taxes are set to zero
		$taxes_are_zero = true;
		foreach ( $cart->get_cart() as $cart_item_key => $cart_item ) {

			$_product		= apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			
			$price_incl_taxes = wc_price( wc_get_price_including_tax( $_product ) );
			$price_excl_taxes = wc_price( wc_get_price_excluding_tax( $_product ) );

 	 		if ( $price_excl_taxes != $price_incl_taxes ) {
 	 			$taxes_are_zero = false;
 	 			break;
 	 		}
		}
			
 		// loop through all fees to add the correct tax
		foreach ( $cart->fees as $k => $fee ) {

			if ( ! apply_filters( 'woocommerce_de_show_gateway_fees_tax', true, $fee ) ) {
				continue;
			}

			if ( ! $use_split_tax ) {
				$tax             = self::get_highest_tax_rate();
				$calc_tax_result = WC_Tax::calc_tax( $fee->amount, $tax, FALSE );
				// attaching the tax_sum to the fee
				$fee->tax = current( $calc_tax_result );
				$fee->taxes[ key($calc_tax_result) ] = current( $calc_tax_result );
				$fee->tax_data[ key($calc_tax_result) ] = current( $calc_tax_result );
				// re-assign the fee to the cart
				$cart->fees[ $k ] = $fee;
				continue;
			}

			$bypass_digital = FALSE;
			//if ( $fee->id == WGM_Fee::get_cod_fee_id() ) {
			//	$bypass_digital = TRUE;
			//}

			// getting the split taxes for the fee
			$taxes = WGM_Tax::calculate_split_rate( $fee->amount, $cart, $bypass_digital, $fee->id, 'fee' );

			// calculating the tax-sum and adding the tax-positions to the fee
			foreach ( $taxes[ 'rates' ] as $tax_id => $tax ) {
				
				if ( ! $taxes_are_zero ) {
					$fee->taxes[ $tax_id ]    = $tax[ 'sum' ];
					$fee->tax_data[ $tax_id ] = $tax[ 'sum' ];
				}

			}

			// attaching the tax_sum to the fee
			$fee->tax = ( ! $taxes_are_zero ) ? $taxes[ 'sum' ] : 0;

			// re-assign the fee to the cart
			$cart->fees[ $k ] = $fee;
		}

	}

	/**
	 * Adding the split taxes to fee order_item which is called
	 * in get_order_item_totals() for thankyou-page, email-template, ..
	 *
	 * @author  Chrico
	 *
	 * @wp-hook woocommerce_get_order_item_totals
	 *
	 * @param   array $items    contains all order items for display
	 * @param   WC_Order $order contains the complete order-Object
	 *
	 * @return  array $items
	 */
	public static function add_tax_string_to_fee_order_item( $items, WC_Order $order ) {

		if( WGM_Tax::is_kur() ){
			return $items;
		}

		if ( is_a( $order, 'WC_Order_Refund' ) ) {
			$parent_id = $order->get_parent_id();
			$order = wc_get_order( $parent_id );
		}

        $use_split_tax = get_option( WGM_Helper::get_wgm_option( 'wgm_use_split_tax' ), 'on' );

        if( $use_split_tax == 'off' ){
            return $items;
        }

		// looping through all fees to fix the text-string which is in "value"
		foreach( $order->get_fees() as $key => $fee ) {

			if ( ! apply_filters( 'woocommerce_de_show_gateway_fees_tax', true, $fee ) ) {
				continue;
			}

			// in $items the fee is saved with {fee_$key)
			$search_key         = 'fee_' . $key;

			if ( !array_key_exists( $search_key, $items ) ) {
				continue;
			}

			$fee_id = sanitize_title_with_dashes( $fee[ 'name' ] );
			$bypass_digital = FALSE;
			//if ( $fee_id == WGM_Fee::get_cod_fee_id() )
			//	$bypass_digital = TRUE;
			$taxes = WGM_Tax::calculate_split_rate( $fee[ 'line_total' ], $order, $bypass_digital, $fee_id, 'fee' );

			// append the tax-messages to the value
			$items[ $search_key ][ 'value' ] .= WGM_Tax::get_split_tax_html( $taxes, get_option( 'woocommerce_tax_display_cart' ) );

		}

		return $items;
	}

	/**
	 * Adds the fee-taxes to the total sum on cart, review-order and second-checkout
	 * WooCommerce only calculates: cart_contents_total + tax_total + shipping_tax_total + shipping_total - discount_total + fee_total
	 *
	 * @author  ChriCo
	 *
	 * @wp-hook woocommerce_calculated_total
	 *
	 * @param   int $total
	 * @param   WC_Cart $cart
	 *
	 * @return  int $total
	 */
	public static function add_fee_taxes_to_total_sum( $total, WC_Cart $cart ) {
		
		if( WGM_Tax::is_kur() ){
			return $total;
		}

		foreach( $cart->get_fees() as $fee ){
			
			if ( ! apply_filters( 'woocommerce_de_show_gateway_fees_tax', true, $fee ) ) {
				continue;
			}

			$total = $total + $fee->tax;
		}
		return $total;
	}

	/**
	 * Adding the Fee taxes to the cart total taxes string (incl./excl. taxes).
	 * The key of the taxes is the {rate_id} (unique id of database-column)
	 *
	 * @author  ChriCo
	 *
	 * @wp-hook woocommerce_cart_get_taxes
	 *
	 * @param   array $taxes
	 * @param   WC_Cart $cart
	 *
	 * @return  array $taxes
	 */
	public static function add_fee_to_cart_tax_totals( $taxes, WC_Cart $cart ){

		if( WGM_Tax::is_kur() ){
			return $taxes;
		}
		
		// looping through all fees in cart
		foreach ( $cart->get_fees() as $fee ) {
			
			if ( ! apply_filters( 'woocommerce_de_show_gateway_fees_tax', true, $fee ) ) {
				continue;
			}

			if ( ! empty( $fee->tax_data ) ) {
				// if tax is not empty, loop through all taxes and add them to taxes array
				foreach ( $fee->tax_data as $rate_id => $tax ) {
					if ( !array_key_exists( $rate_id, $taxes ) ) {
						$taxes[ $rate_id ] = 0;
					}
					$taxes[ $rate_id ] += $tax;
				}
			}
		}

		return $taxes;
	}


	/**
	 * Adds the fee taxes to the tax_totals-array.
	 * The key of $tax_totals is the unique WC_Tax::get_rate_code( $rate_id );
	 *
	 * @author  ChriCo
	 *
	 * @wp-hook woocommerce_order_tax_totals
	 *
	 * @param   array $tax_totals
	 * @param   WC_Order $order
	 *
	 * @return  array $tax_totals
	 */
	public static function add_fee_to_order_tax_totals( $tax_totals, $order ){

		if( WGM_Tax::is_kur() ){
			return $tax_totals;
		}

		if ( is_a( $order, 'WC_Order_Refund' ) ) {
			$parent_id = $order->get_parent_id();
			$order = wc_get_order( $parent_id );
		}

        $use_split_tax = get_option( WGM_Helper::get_wgm_option( 'wgm_use_split_tax' ), 'on' );

        if( $use_split_tax == 'off' ){
            return $tax_totals;
        }

		// looping through all existing fees
		foreach( $order->get_fees() as $key => $fee ) {

			if ( ! apply_filters( 'woocommerce_de_show_gateway_fees_tax', true, $fee ) ) {
				continue;
			}

			$fee_id = sanitize_title_with_dashes( $fee[ 'name' ] );
			$bypass_digital = FALSE;
			//if ( $fee_id == WGM_Fee::get_cod_fee_id() )
			//	$bypass_digital = TRUE;

			//$order->calculate_totals();
			$taxes = WGM_Tax::calculate_split_rate( $fee['line_total'], $order, $bypass_digital, $fee_id, 'fee' );

			// looping through all found taxes
			foreach( $taxes[ 'rates' ] as $rate_id => $item ) {

				// getting the unique rate_code
				$rate_code = WC_Tax::get_rate_code( $rate_id );

				if ( !array_key_exists( $rate_code, $tax_totals ) ) {
					continue;
				}

				// add the new amount to the current amount
				$new_amount                         = $tax_totals[ $rate_code ]->amount + $item[ 'sum' ];
				$tax_totals[ $rate_code ]->amount   = $new_amount;

				// create the new formatted amount
				$tax_totals[ $rate_code ]->formatted_amount = wc_price(
					wc_round_tax_total( $new_amount ),
					array('currency' => $order->get_currency() )
				);

			}
		}

		return $tax_totals;
	}

	public static function get_cod_fee_id() {

		$available_gateways = WC()->payment_gateways->get_available_payment_gateways();
		if ( isset( $available_gateways[ 'cash_on_delivery' ] ) )
			$cod_gateway        = $available_gateways[ 'cash_on_delivery' ];
		else
			return FALSE;

		return sanitize_title( $cod_gateway->title );
	}
}