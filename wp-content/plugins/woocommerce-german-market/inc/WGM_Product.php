<?php

class WGM_Product {

	public static function init(  ) {
		add_filter( 'product_type_options',                                     array( 'WGM_Product', 'register_product_type') );
		add_action( 'woocommerce_variation_options',                            array( 'WGM_Product', 'add_variant_product_type'), 10, 3 );
		add_action( 'woocommerce_update_product_variation',                     array( 'WGM_Product', 'save_variant_product_type'), 10, 3 );
		add_action( 'woocommerce_create_product_variation',                     array( 'WGM_Product', 'save_variant_product_type'), 10, 3 );
		add_action( 'save_post',                                                array( 'WGM_Product', 'save_product_digital_type'), 10, 2 );


	}

	/**
	 * @param array $types
	 * @wp-hook product_type_options
	 * @return array $tye
	 */
	public static function register_product_type(array $types){

		$types[ 'digital' ] = array(
				'id'            => '_digital',
				'wrapper_class' => 'show_if_simple',
				'label'         => __( 'Digital', 'woocommerce-german-market' ),
				'description'   => __( 'Only products with this marker will be treated as “digital” in the context of the EU Consumer Rights Directive from June 13, 2014.', 'woocommerce-german-market' ),
				'default'       => 'no'
			);

		return $types;
	}

	/**
	 * @param int $id
	 * @param Post $post
	 * @wp-hook save_post
	 */
	public static function save_product_digital_type( $id, $post ){
		
		if( $post->post_type != 'product' ) {
			return;
		}

		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) || ( defined( 'DOING_AJAX' ) && DOING_AJAX) || isset( $_REQUEST[ 'bulk_edit' ] ) ) {
			return;
		}

		if( isset( $_REQUEST[ '_digital' ] ) ) {
			update_post_meta($id, '_digital', 'yes' );
		} else {
			update_post_meta($id, '_digital', 'no' );
		}
	}

	/**
	 * Adds Digital checkbox to variation meta box
	 * @param $loop
	 * @param $variation_data
	 * @param $variation
	 * @wp-hook woocommerce_variation_options
	 */
	public static function add_variant_product_type( $loop, $variation_data, $variation ) {
		$_digital = get_post_meta( $variation->ID, '_digital', true );

		?>
		<label>
			<input type="checkbox" id="_digital" class="checkbox variable_is_digital" name="variable_is_digital[<?php echo $loop; ?>]" <?php checked( isset( $_digital ) ? $_digital : '', 'yes' ); ?> />
				<?php _e( 'Digital', 'woocommerce-german-market' ); ?>
				<a class="tips" data-tip="<?php esc_attr_e( 'Only products with this marker will be treated as “digital” in the context of the EU Consumer Rights Directive from June 13, 2014.', 'woocommerce-german-market' ); ?>" href="#">[?]</a>
			</label>

	<?php
	}

	/**
	 *  Save the digital setting for variations
	 * @param $var_id
	 * @wp-hook woocommerce_update_product_variation
	 * @wp-hook woocommerce_create_product_variation
	 *
	 */
	public static function save_variant_product_type( $var_id ){

		if( ! isset($_POST['variable_post_id'] ) ){
			return;
		}

		$variable_post_id   = $_POST['variable_post_id'];
		$max_loop           = max( array_keys( $_POST['variable_post_id'] ) );

		for ( $i = 0; $i <= $max_loop; $i ++ ) {

			if ( ! isset( $variable_post_id[ $i ] ) ) {
				continue;
			}

			$variable_is_virtual = isset( $_POST['variable_is_digital'] ) ? $_POST['variable_is_digital'] : array();

			$variation_id = absint( $variable_post_id[ $i ] );

			if( $variation_id == $var_id ){
				$is_digital = isset( $variable_is_virtual[ $i ] ) ? 'yes' : 'no';
				update_post_meta( $var_id, '_digital', $is_digital );
			}
		}
	}
}
