<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'German_Market_Email_Sepa' ) ) :

    if( ! class_exists( 'WC_Email' ) ){
        // Initialize mailer
        WC()->mailer();
    }

    /**
     * Sepa Mandat Email
     *
     * An email sent to the customer when a new order is received
     *
     * @class 		WGM_Email_Sepa
     * @version		1.0
     * @extends 	WC_Email
     */
    class German_Market_Email_Sepa extends WC_Email {

        /**
         * Constructor
         */
        function __construct() {

            $this->id 				    = 'german_market_email_sepa';
            $this->title 		    	= __( 'SEPA Direct Debit Mandate', 'woocommerce-german-market' );
            $this->description		    = __( 'SEPA Direct Debit Mandate sent to customers', 'woocommerce-german-market' );

            $this->heading 			    = __( 'SEPA Direct Debit Mandate', 'woocommerce-german-market' );
            $this->subject      	    = __( 'SEPA Direct Debit Mandate', 'woocommerce-german-market' );

            $this->args                 = array();

            $this->settings_type        = 'html';

            // Call parent constructor
            parent::__construct();
        }

	    /**
	     * get_type function.
	     *
	     * @return string
	     */
	    public function get_email_type() {

		    return $this->settings_type;
	    }

        /**
         * trigger function.
         *
         * @access public
         * @return void
         */
        function trigger( $order_id ) {

            if ( $order_id ) {
                $this->object 		= wc_get_order( $order_id );
                $this->recipient	= $this->object->get_billing_email();
            }

            if ( ! $this->get_recipient() ) {
                return;
            }

            $content = $this->get_content();

            $headers = $this->get_headers();
            $headers = apply_filters( 'woocommerce_de_header_sepa_mail', $headers );

            $this->send( $this->get_recipient(), $this->get_subject(), $content, $headers, $this->get_attachments() );
        }

        /**
        * Set Args
        **/
        function set_args( $args ) {
            $this->args = $args;

            if ( isset( $args[ 'email_subject' ] ) ) {
                $this->subject = $args[ 'email_subject' ];
            }

            if ( isset( $args[ 'email_heading' ] ) ) {
                $this->heading = $args[ 'email_heading' ];
            }

            if ( isset( $args[ 'email_type' ] ) ) {
                $this->settings_type = $args[ 'email_type' ];
            }
        }

        /**
         * get_content_html function.
         *
         * @access public
         * @return string
         */
        function get_content_html() {
            
            // Content, get saved text in backend and replace args
            $content = WGM_Sepa_Direct_Debit::generatre_mandate_preview( $this->args, $this->args[ 'mandate_id' ], $this->args[ 'date' ] );

            $template_file = WGM_Template::locate_template( 'emails/sepa-mandate.php' );

            ob_start();

             // extract needed vars
            extract( array(
                'content'       => $content,
                'email_heading' => $this->heading,
            ) );

            include( $template_file );

            return apply_filters( 'german_market_sepa_email_get_content_html', ob_get_clean(), $content, $email_heading, $this->args, $this->args[ 'mandate_id' ], $this->args[ 'date' ] );

        }

        /**
         * get_content_plain function.
         *
         * @access public
         * @return string
         */
        function get_content_plain() {

            // Content, get saved text in backend and replace args
            $content = WGM_Sepa_Direct_Debit::generatre_mandate_preview( $this->args, $this->args[ 'mandate_id' ], $this->args[ 'date' ] );

            $template_file = WGM_Template::locate_template( 'emails/plain/sepa-mandate.php' );

            ob_start();

             // extract needed vars
            extract( array(
                'content'       => $content,
                'email_heading' => $this->heading,
            ) );

            include( $template_file );

            return apply_filters( 'german_market_sepa_email_get_content_plain', ob_get_clean(), $content, $email_heading, $this->args, $this->args[ 'mandate_id' ], $this->args[ 'date' ] );
        }
    }

endif;

return new German_Market_Email_Sepa();
