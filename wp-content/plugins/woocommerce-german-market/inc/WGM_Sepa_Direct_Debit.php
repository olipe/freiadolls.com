<?php

/**
 * Class WGM_Sepa_Direct_Debit
 *
 * German Market Gateway SEPA Direct Debit
 *
 * @author MarketPress
 */
class WGM_Sepa_Direct_Debit {

	/**
	 * @var WGM_Sepa_Direct_Debit
	 * @since v3.3
	 */
	private static $instance = null;
	
	/**
	* Singletone get_instance
	*
	* @static
	* @return WGM_Sepa_Direct_Debit
	*/
	public static function get_instance() {
		if ( self::$instance == NULL) {
			self::$instance = new WGM_Sepa_Direct_Debit();	
		}
		return self::$instance;
	}

	/**
	* Singletone constructor
	*
	* @access private
	*/
	private function __construct() {
 		
		if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
			return;
		}
		
		require_once dirname( Woocommerce_German_Market::$plugin_filename ) . '/gateways/WGM_Gateway_Sepa_Direct_Debit.php';
		
		$sdd_settings = get_option( 'woocommerce_german_market_sepa_direct_debit_settings' );

		$costs = $sdd_settings[ 'woocommerce_german_market_sepa_direct_debit_fee' ];
		if ( floatval( str_replace( ',', '.', $costs ) ) > 0.0 ) {
			WGM_Gateways::set_gateway_fee( 'german_market_sepa_direct_debit' , $sdd_settings[ 'woocommerce_german_market_sepa_direct_debit_fee' ] );
		}

		add_filter( 'woocommerce_payment_gateways', array( $this, 'german_market_add_sepa_direct_debit' ) );

		/**
		/* validation of additional checkout fields
		**/
		if ( get_option( 'woocommerce_de_secondcheckout', 'off' ) == 'on' ) {
			add_filter( 'gm_checkout_validation_first_checkout', array( 'WGM_Gateway_Sepa_Direct_Debit', 'validate_required_fields' ) );
		}

		if ( $sdd_settings[ 'enabled' ] == 'yes' && $sdd_settings[ 'checkbox_confirmation' ] == 'activated' ) {

			add_filter( 'woocommerce_de_review_order_after_submit', array( 'WGM_Gateway_Sepa_Direct_Debit', 'checkout_field_checkbox' ) );

			if ( get_option( 'woocommerce_de_secondcheckout', 'off' ) == 'on' ) {

		   		add_filter( 'gm_checkout_validation_fields_second_checkout', array( 'WGM_Gateway_Sepa_Direct_Debit', 'checkout_field_checkbox_validation' ) );

		   	} else {

		   		add_filter( 'woocommerce_after_checkout_validation', array( 'WGM_Gateway_Sepa_Direct_Debit', 'checkout_field_checkbox_validation' ) );

		   	}

		}

		// ajax
		if ( $sdd_settings[ 'enabled' ] == 'yes' ) {
			add_action( 'wp_ajax_gm_sepa_direct_debit_mandate_preview', 		array( $this, 'ajax_mandate_preview' ) );
			add_action( 'wp_ajax_nopriv_gm_sepa_direct_debit_mandate_preview', 	array( $this, 'ajax_mandate_preview' ) );
		}
		
		if ( is_admin() ) {
			add_action( 'current_screen', array( $this, 'load_gateway_on_shop_order' ) );
		}
		
	}

	/**
	* Load Gateway on shop order screen
	*
	* @wp-hook current_screen
	* @return void
	**/
	public function load_gateway_on_shop_order() {

		$screen = get_current_screen();

		if ( $screen->id == 'edit-shop_order' || $screen->id == 'shop_order' ) {
			if ( WGM_Gateway_Sepa_Direct_Debit::$instances == 0 ) {
				$new_gateway = new WGM_Gateway_Sepa_Direct_Debit();
			}
		}
	}

	/**
	* Ajax
	*
	* @since GM 3.3
	* @wp-hook wp_ajax_gm_sepa_direct_debit_mandate_preview
	* @return void
	**/
	public function ajax_mandate_preview() {
		
		if ( ! ( isset( $_REQUEST[ 'nonce' ] ) && wp_verify_nonce( $_REQUEST[ 'nonce' ], 'gm-sepa-direct-debit' ) ) ) {

			echo __( 'There was an error generating the mandate preview. Please reload the page and try again', 'woocommerce-german-market' );

		} else {

			unset( $_REQUEST[ 'nonce' ] );

			// close button
			?><div class="close"><?php echo apply_filters( 'german_market_sepa_close_mandate_preview', __( 'Close', 'woocommerce-german-market' ) ); ?></div><?php

			// mandate text
			echo '<div class="gm-sepa-mandate-preview-inner">' . self::generatre_mandate_preview( $_REQUEST ) . '</div>';

		}

		exit();
	}

	/**
	* Get Ajax preview
	*
	* @since GM 3.3
	* @param Array $args
	* @return String
	**/
	public static function generatre_mandate_preview( $args, $mandate_id = false, $date = false ) {

		$sdd_settings = get_option( 'woocommerce_german_market_sepa_direct_debit_settings' );

		$mandate_text = $sdd_settings[ 'direct_debit_mandate' ];

		$search = array(
			'[creditor_information]',
			'[creditor_identifier]',
			'[creditor_account_holder]',
			'[creditor_iban]',
			'[creditor_bic]',
			'[mandate_id]',
			'[street]',
			'[city]',
			'[postcode]',
			'[country]',
			'[date]',
			'[account_holder]',
			'[account_iban]',
			'[account_bic]',
		);

		$replace = array(
			$sdd_settings[ 'creditor_information' ],
			$sdd_settings[ 'creditor_identifier' ],
			$sdd_settings[ 'creditor_account_holder' ],
			$sdd_settings[ 'iban' ],
			$sdd_settings[ 'bic' ],
			$mandate_id ? $mandate_id : __( 'Will be communicated separately', 'woocommerce-german-market' ),
			$args[ 'street' ],
			$args[ 'city' ],
			$args[ 'zip' ],
			$args[ 'country' ],
			$date ? $date :  date_i18n( get_option( 'date_format' ), time() ),
			$args[ 'holder' ],
			$args[ 'iban' ],
			$args[ 'bic' ],
		);

		$mandate_preview = apply_filters( 'the_content', str_replace( $search, $replace, $mandate_text ) );

		return $mandate_preview;

	}

	/**
	* Add Gateway
	*
	* @since GM 3.3
	* @wp-hook woocommerce_payment_gateways
	* @param Array $gateway
	* @return Array
	**/
	public function german_market_add_sepa_direct_debit( $gateways ) {
		$gateways[] = 'WGM_Gateway_Sepa_Direct_Debit';
		return $gateways;
	}


}
